package com.rubenkuilder.wematextrecognizer

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.rubenkuilder.wematextrecognizer.databinding.FragmentCheckBinding

class CheckFragment : Fragment() {
    private lateinit var binding: FragmentCheckBinding
    private var saveNext = ""
    private var saveSex = ""
    private var saveNationality = ""

    private var documentNo = ""
    private var surname = ""
    private var givenNames = ""
    private var sex = ""
    private var nationality = ""
    private var dateOfBirth = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_check, container, false)

        var args = CheckFragmentArgs.fromBundle(requireArguments())

        // create a StringBuilder object
        // with a String pass as parameter
        val str = StringBuilder(args.result)

        // print string
        Log.i("CheckFragment", "String contains = $str")

        // get index of string For
        val index = str.indexOf("sex")

        // print results
        Log.i("CheckFragment", "index of string 'For' = $index")

        if(index != -1) {
            str.insert(index, "\n")
        }

        Log.i("CheckFragment", "New string contains = $str")


        var lines = args.result.lines()
        var i = 0
        while(i < lines.size) {
            if(lines[i].contains("documentnummer") || lines[i].contains("document no")) {
                documentNo = lines[i+1]

                Log.i("CheckFragment", "documentNo: $documentNo")
                Log.i("CheckFragment", "---")
            } else if(lines[i].contains("naam") || lines[i].contains("surname")) {
                surname = lines[i + 1]

                Log.i("CheckFragment", "surname: $surname")
                Log.i("CheckFragment", "---")
            } else if(lines[i].contains("voornamen") || lines[i].contains("given names")) {
                givenNames = lines[i + 1]

                Log.i("CheckFragment", "givenNames: $givenNames")
                Log.i("CheckFragment", "---")
            } else if(lines[i].contains("geslacht") || lines[i].contains("sex")) {
                sex = lines[i + 2]

                Log.i("CheckFragment", "sex: $sex")
                Log.i("CheckFragment", "---")
            } else if(lines[i].contains("nationaliteit") || lines[i].contains("nationality")) {
                nationality = lines[i + 2]

                Log.i("CheckFragment", "nationality: $nationality")
                Log.i("CheckFragment", "---")
            } else if(lines[i].contains("geboortedatum") || lines[i].contains("date of birth")) {
                dateOfBirth = lines[i + 1]

                Log.i("CheckFragment", "dateOfBirth: $dateOfBirth")
                Log.i("CheckFragment", "---")
            }

            i++
        }

        binding.documentNoInput.setText(documentNo, TextView.BufferType.EDITABLE)
        binding.surnameInput.setText(surname, TextView.BufferType.EDITABLE)
        binding.givenNamesInput.setText(givenNames, TextView.BufferType.EDITABLE)
        binding.sexInput.setText(sex, TextView.BufferType.EDITABLE)
        binding.nationalityInput.setText(nationality, TextView.BufferType.EDITABLE)
        binding.dateOfBirthInput.setText(dateOfBirth, TextView.BufferType.EDITABLE)

        return binding.root
    }
}