package com.rubenkuilder.wematextrecognizer

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.hardware.display.DisplayManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.util.Size
import android.view.Surface
import android.widget.Toast
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.lang.Exception
import java.lang.Math.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {
    private var imageCapture: ImageCapture? = null
    private var imageAnalyzer: ImageAnalysis? = null

    private lateinit var cameraExecutor: ExecutorService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

//    private fun takePhoto() {
//        // Get a stable reference of the modifiable image capture use case
//        if(imageCapture == null) {
//            return
//        }
//
//        // Set up image capture listener, which is triggered after photo has
//        // been taken
//        imageCapture!!.takePicture(
//                ContextCompat.getMainExecutor(this), object : ImageCapture.OnImageCapturedCallback() {
//            override fun onError(exc: ImageCaptureException) {
//                Log.e(TAG, "Photo capture failed: ${exc.message}", exc)
//            }
//
//            @SuppressLint("UnsafeExperimentalUsageError")
//            override fun onCaptureSuccess(imageProxy: ImageProxy) {
//                val msg = "Photo capture succeeded"
//                Toast.makeText(baseContext, msg, Toast.LENGTH_LONG).show()
//                Log.d(TAG, msg)
//
//                Log.d("Test - rotationdegrees", imageProxy.imageInfo.toString())
//
//                val mediaImage = imageProxy.image
//                if (mediaImage != null) {
//                    val image = InputImage.fromMediaImage(mediaImage, imageProxy.imageInfo.rotationDegrees)
//                    val recognizer = TextRecognition.getClient()
//
//                    recognizer.process(image)
//                        .addOnSuccessListener { visionText ->
//                            Toast.makeText(baseContext, visionText.text, Toast.LENGTH_LONG).show()
//                            Log.d(TAG, visionText.text)
//
//                            imageProxy.close()
//                        }
//                        .addOnFailureListener { e ->
//                            Log.e(TAG, "Recognizer failed: ${e.message}", e)
//
//                            imageProxy.close()
//                        }
//                }
//            }
//        })
//    }
//
//
//
//    private fun startCamera() {
//        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)
//
//        cameraProviderFuture.addListener(Runnable {
//            // Used to bind the lifecycle of cameras to the lifecycle owner
//            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()
//
//
//
//            // Get screen metrics used to setup camera for full screen resolution
//            val metrics = DisplayMetrics().also { viewFinder.display.getRealMetrics(it) }
//            Log.d(TAG, "Screen metrics: ${metrics.widthPixels} x ${metrics.heightPixels}")
//
//            val screenAspectRatio = aspectRatio(metrics.widthPixels, metrics.heightPixels)
//            Log.d(TAG, "Preview aspect ratio: $screenAspectRatio")
//
//            val rotation = viewFinder.display.rotation
//            Log.d("Test - rotation", rotation.toString())
//
//
//
//
//            // Select back camera as a default
//            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
//
//            // Preview
//            val preview = Preview.Builder()
//                    .setTargetAspectRatio(screenAspectRatio)
//                    .setTargetRotation(rotation)
//                    .build()
//                    .also {
//                        it.setSurfaceProvider(viewFinder.createSurfaceProvider())
//                    }
//
//            imageCapture = ImageCapture.Builder()
//                    .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
//                    .setTargetAspectRatio(screenAspectRatio)
//                    .setTargetRotation(rotation)
//                    .build()
//
//            imageAnalyzer = ImageAnalysis.Builder()
//                    .setTargetAspectRatio(screenAspectRatio)
//                    .setTargetRotation(rotation)
//                    .build()
//            Log.i("Test - imageAnalyzer", imageAnalyzer!!.targetRotation.toString())
//
//
//            try {
//                // Unbind use cases before rebinding
//                cameraProvider.unbindAll()
//
//                // Bind use cases to camera
//                cameraProvider.bindToLifecycle(
//                        this, cameraSelector, imageAnalyzer, preview, imageCapture)
//
//            } catch(exc: Exception) {
//                Log.e(TAG, "Use case binding failed", exc)
//            }
//
//        }, ContextCompat.getMainExecutor(this))
//    }
//
//    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
//        ContextCompat.checkSelfPermission(
//                baseContext, it) == PackageManager.PERMISSION_GRANTED
//    }
//
//    override fun onRequestPermissionsResult(
//            requestCode: Int, permissions: Array<String>, grantResults:
//            IntArray) {
//        if (requestCode == REQUEST_CODE_PERMISSIONS) {
//            if (allPermissionsGranted()) {
//                startCamera()
//            } else {
//                Toast.makeText(this,
//                        "Permissions not granted by the user.",
//                        Toast.LENGTH_SHORT).show()
//                finish()
//            }
//        }
//    }
//
//    /**
//     *  [androidx.camera.core.ImageAnalysisConfig] requires enum value of
//     *  [androidx.camera.core.AspectRatio]. Currently it has values of 4:3 & 16:9.
//     *
//     *  Detecting the most suitable ratio for dimensions provided in @params by counting absolute
//     *  of preview ratio to one of the provided values.
//     *
//     *  @param width - preview width
//     *  @param height - preview height
//     *  @return suitable aspect ratio
//     */
//    private fun aspectRatio(width: Int, height: Int): Int {
//        val previewRatio = max(width, height).toDouble() / min(width, height)
//        if (abs(previewRatio - RATIO_4_3_VALUE) <= abs(previewRatio - RATIO_16_9_VALUE)) {
//            return AspectRatio.RATIO_4_3
//        }
//        return AspectRatio.RATIO_16_9
//    }
//
//    companion object {
//        private const val TAG = "CameraXBasic"
//        private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
//        private const val REQUEST_CODE_PERMISSIONS = 10
//        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
//        private const val RATIO_4_3_VALUE = 4.0 / 3.0
//        private const val RATIO_16_9_VALUE = 16.0 / 9.0
//    }
}