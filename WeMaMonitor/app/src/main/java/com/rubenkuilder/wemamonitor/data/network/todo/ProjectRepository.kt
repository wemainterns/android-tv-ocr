package com.rubenkuilder.wemamonitor.data.network.todo

import androidx.lifecycle.MutableLiveData

interface ProjectRepository {
    fun getDoingData(): MutableLiveData<ArrayList<Project>>
    fun getTestingData(): MutableLiveData<ArrayList<Project>>
    fun getDoneData(): MutableLiveData<ArrayList<Project>>
    fun setProjects()
    fun validateProjects()
}