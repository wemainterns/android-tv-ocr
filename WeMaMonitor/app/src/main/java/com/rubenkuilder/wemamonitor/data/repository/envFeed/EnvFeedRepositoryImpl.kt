package com.rubenkuilder.wemamonitor.data.repository.envFeed

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rubenkuilder.wemamonitor.data.db.CurrentWeatherDao
import com.rubenkuilder.wemamonitor.data.db.currentWeather.CurrentWeatherDBMapper
import com.rubenkuilder.wemamonitor.data.db.envFeed.EnvFeedDBMapper
import com.rubenkuilder.wemamonitor.data.db.envFeed.entity.ProductFromEnvPair
import com.rubenkuilder.wemamonitor.data.db.envFeed.entity.ProductToEnvPair
import com.rubenkuilder.wemamonitor.data.domain.currentWeather.CurrentWeather
import com.rubenkuilder.wemamonitor.data.domain.envFeed.EnvFeed
import com.rubenkuilder.wemamonitor.data.network.envFeed.EnvFeedNetworkDataSource
import com.rubenkuilder.wemamonitor.data.network.envFeed.EnvFeedNetworkMapper
import com.rubenkuilder.wemamonitor.data.network.envFeed.response.EnvFeedNetworkEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.ZonedDateTime
import javax.inject.Inject

class EnvFeedRepositoryImpl
@Inject
constructor(
    private val currentWeatherDao: CurrentWeatherDao,
    private val envFeedNetworkDataSource: EnvFeedNetworkDataSource,
    private val envFeedDbMapper: EnvFeedDBMapper,
    private val envFeedNetworkMapper: EnvFeedNetworkMapper
) : EnvFeedRepository {
    private var timeFetched = ZonedDateTime.now().minusHours(1)

    /**
     * Initialize observable
     */
    init {
        envFeedNetworkDataSource.downloadedEnvFeedNetworkEntity.observeForever { newEnvFeed ->
            persistFetchedEnvFeed(newEnvFeed)
        }
    }

    /**
     * Check if data needs to be fetched from API and get environment feed data from database
     */
    override suspend fun getEnvFeed(): LiveData<List<EnvFeed>> {
        return withContext(Dispatchers.IO) {
            initEnvFeedData()

            val myEnvFeedData = MutableLiveData<List<EnvFeed>>(envFeedDbMapper.mapFromProductWithFromEnvListToEnvFeed(currentWeatherDao.getProductWithEnv()))
            myEnvFeedData.value?.forEach {
                Log.i("appDebug2", "myEnvFeedData: $it")
            }
            return@withContext myEnvFeedData
        }
    }

    /**
     * Upsert environment data to database
     *
     * @param fetchedEnvFeed the environment feed fetched in weatherNetworkDataSource
     */
    private fun persistFetchedEnvFeed(fetchedEnvFeed: List<EnvFeedNetworkEntity>) {
        GlobalScope.launch(Dispatchers.IO) {
            Log.i("Test - ForecastRepositoryImpl", fetchedEnvFeed.toString())

            var productInsertedId: Long
            var fromEnvironmentInsertedId: Long
            var toEnvironmentInsertedId: Long

            val envFeedList = envFeedNetworkMapper.mapFromEntityList(fetchedEnvFeed)
            for(product in envFeedList) {
                productInsertedId = currentWeatherDao.upsertProductDBEntity(envFeedDbMapper.mapEnvFeedToProductDBEntity(product))
                val lastProductTemp = currentWeatherDao.loadSingleProduct(productInsertedId)
                Log.i("appDebug", "singleProduct: ${currentWeatherDao.loadSingleProduct(productInsertedId)}")

                for(environment in product.from_env) {
                    fromEnvironmentInsertedId = currentWeatherDao.upsertEnvironmentDBEntity(envFeedDbMapper.mapEnvToEnvironmentDBEntity(environment))

                    val lastFromEnvironmentTemp = currentWeatherDao.loadSingleEnvironment(fromEnvironmentInsertedId)
                    Log.i("appDebug", "fromEnvironmentInsertedId: ${currentWeatherDao.loadSingleEnvironment(fromEnvironmentInsertedId)}")

                    //Insert ProductFromEnvPair with product_id and env_id from above ^
                    currentWeatherDao.upsertProductWithFromEnv(ProductFromEnvPair(
                        product_id = lastProductTemp.product_id,
                        env_id = lastFromEnvironmentTemp.env_id
                    ))
                }
                for(environment in product.to_env) {
                    toEnvironmentInsertedId = currentWeatherDao.upsertEnvironmentDBEntity(envFeedDbMapper.mapEnvToEnvironmentDBEntity(environment))

                    val lastToEnvironmentTemp = currentWeatherDao.loadSingleEnvironment(toEnvironmentInsertedId)
                    Log.i("appDebug", "toEnvironmentInsertedId: ${currentWeatherDao.loadSingleEnvironment(toEnvironmentInsertedId)}")

                    //Insert ProductFromEnvPair with product_id and env_id from above ^
                    currentWeatherDao.upsertProductWithToEnv(ProductToEnvPair(
                        product_id = lastProductTemp.product_id,
                        env_id = lastToEnvironmentTemp.env_id
                    ))
                }

                Log.i("appDebug", "-----------")
            }
        }
    }

    /**
     * Check if fetch is needed. If fetch is needed, fetch data and set timeFetched
     */
    private suspend fun initEnvFeedData() {
        if(isFetchCurrentNeeded()) {
            fetchEnvFeed()

            //Set new fetched time
            timeFetched = ZonedDateTime.now()
        }
    }

    /**
     * Fetch complete environment feed
     */
    private suspend fun fetchEnvFeed() {
        envFeedNetworkDataSource.fetchEnvFeed()
    }

    /**
     * Check if fetching of API data is currently needed
     *
     * @return return if timeFetched is before thirty minutes ago
     */
    private fun isFetchCurrentNeeded(): Boolean {
        val thirtyMinutesAgo = ZonedDateTime.now().minusMinutes(30)

        return timeFetched.isBefore(thirtyMinutesAgo)
    }
}