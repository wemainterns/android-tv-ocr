package com.rubenkuilder.wemamonitor.data.network.clientQuote

import androidx.lifecycle.MutableLiveData
import javax.inject.Inject
import kotlin.random.Random

class QuoteRepositoryImpl
@Inject
constructor(): QuoteRepository {
    private var quotes = ArrayList<Quote>()

    /**
     * Pretend to get data from a webservice or online source
     */
    override fun getQuote(): MutableLiveData<Quote> {
        setQuotes()

        val randomInt = Random.nextInt(0, quotes.size - 1)
        val data = MutableLiveData<Quote>()
        data.value = quotes[randomInt]

        return data
    }

    /**
     * Add quotes to quotes list
     */
    override fun setQuotes() {
        quotes.add(Quote("Wesley's kantoor", "https://www.pdn.cam.ac.uk/teaching/teaching_images/iTunes-U-1.4-for-iOS-app-icon-small.png/image_preview", "I don't have dreams, I have goals."))
        quotes.add(Quote("Sinterklaas", "https://www.pdn.cam.ac.uk/teaching/teaching_images/iTunes-U-1.4-for-iOS-app-icon-small.png/image_preview", "Dream bigger."))
        quotes.add(Quote("Kerstman", "https://www.pdn.cam.ac.uk/teaching/teaching_images/iTunes-U-1.4-for-iOS-app-icon-small.png/image_preview", "Your limitation—it's only your imagination."))
    }
}