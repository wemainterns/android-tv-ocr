package com.rubenkuilder.wemamonitor.data.db.envFeed.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "env_feed_product")
data class ProductDBEntity(
    @PrimaryKey(autoGenerate = false)
    val product_id: String,
    val product: String,
    val employee: String,
    val date: String,
    val changelog: String
)