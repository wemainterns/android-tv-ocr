package com.rubenkuilder.wemamonitor.ui

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.rubenkuilder.wemamonitor.R
import com.rubenkuilder.wemamonitor.utilities.Utils.meetingDateTime
import com.rubenkuilder.wemamonitor.ui.timer.TimerTimerTask
import com.rubenkuilder.wemamonitor.data.network.welcome.Meeting
import com.rubenkuilder.wemamonitor.ui.welcome.WelcomeTimerTask
import com.rubenkuilder.wemamonitor.utilities.*
import dagger.hilt.android.AndroidEntryPoint
import java.time.*
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * MainActivity that handles navigation to the timer and welcome fragments
 */
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: MainActivityViewModel
    lateinit var navController: NavController
    lateinit var timer: Timer

    private val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")
    private val currentDateTime = LocalDateTime.of(LocalDate.now(ZoneId.of("Europe/Amsterdam")), LocalTime.now(ZoneId.of("Europe/Amsterdam")))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.myNavHostFragment) as NavHostFragment
        navController = navHostFragment.navController
        timer = Timer()

        viewModel.meeting.observe(this, androidx.lifecycle.Observer { meeting ->
            autoNavigateWelcome(meeting)
        })
    }

    /**
     * Initialize autoNavigateTimer here so that the activity is running and the timerFragment can run onBackPressed
     */
    override fun onResume() {
        super.onResume()

        autoNavigateTimer()
    }

    /**
     * Timer which handles navigation to testTimer fragment
     */
    private fun autoNavigateTimer() {
        Log.i("Test - MainActivity", "autoNavigateTimer")

        // Set times
        val testTime: Date = Date.from(ZonedDateTime.of(LocalDate.now(ZoneId.of("Europe/Amsterdam")), LocalTime.parse("${String.format("%02d",
            TEST_START_HOUR
        )}:${String.format("%02d",
            TEST_START_MINUTE
        )}:00"), ZoneId.of("Europe/Amsterdam")).toInstant())
        val break1Time: Date = Date.from(ZonedDateTime.of(LocalDate.now(ZoneId.of("Europe/Amsterdam")), LocalTime.parse("${String.format("%02d",
            BREAK1_START_HOUR
        )}:${String.format("%02d",
            BREAK1_START_MINUTE
        )}:00"), ZoneId.of("Europe/Amsterdam")).toInstant())
        val break2Time: Date = Date.from(ZonedDateTime.of(LocalDate.now(ZoneId.of("Europe/Amsterdam")), LocalTime.parse("${String.format("%02d",
            BREAK2_START_HOUR
        )}:${String.format("%02d",
            BREAK2_START_MINUTE
        )}:00"), ZoneId.of("Europe/Amsterdam")).toInstant())
        val break3Time: Date = Date.from(ZonedDateTime.of(LocalDate.now(ZoneId.of("Europe/Amsterdam")), LocalTime.parse("${String.format("%02d",
            BREAK3_START_HOUR
        )}:${String.format("%02d",
            BREAK3_START_MINUTE
        )}:00"), ZoneId.of("Europe/Amsterdam")).toInstant())
        val orderReminderTime: Date = Date.from(ZonedDateTime.of(LocalDate.now(ZoneId.of("Europe/Amsterdam")), LocalTime.parse("${String.format("%02d",
            ORDERREMINDER_START_HOUR
        )}:${String.format("%02d",
            ORDERREMINDER_START_MINUTE
        )}:00"), ZoneId.of("Europe/Amsterdam")).toInstant())

        Log.i("Test - MainActivity", "$testTime")

        // Set tasks
        val testTask: TimerTask =
            TimerTimerTask(
                "test",
                navController
            ) // creating timer task
        val break1Task: TimerTask =
            TimerTimerTask(
                "break1",
                navController
            ) // creating timer task
        val break2Task: TimerTask =
            TimerTimerTask(
                "break2",
                navController
            ) // creating timer task
        val break3Task: TimerTask =
            TimerTimerTask(
                "break3",
                navController
            ) // creating timer task
        val orderReminderTask: TimerTask =
            TimerTimerTask(
                "orderReminder",
                navController
            ) // creating timer task

        // scheduling the task for repeated fixed-delay execution, beginning after the specified delay
        timer.schedule(testTask, testTime)
        timer.schedule(break1Task, break1Time)
        timer.schedule(break2Task, break2Time)
        timer.schedule(break3Task, break3Time)
        if(currentDateTime.dayOfWeek == DayOfWeek.MONDAY) {
            timer.schedule(orderReminderTask, orderReminderTime)
        }
    }

    private fun autoNavigateWelcome(meeting: Meeting) {
        meetingDateTime = LocalDateTime.parse(meeting.meetingTime, formatter)
        val meetingTime: Date = Date.from(ZonedDateTime.of(meetingDateTime!!.minusMinutes(
            SHOW_X_MIN_BEFORE_CLIENT_MEETING
        ), ZoneId.of("Europe/Amsterdam")).toInstant())

        //Set TimerTask and pass navOptions (clientName and clientImage)
        val welcomeTimerTask: TimerTask =
            WelcomeTimerTask(
                meeting.clientName,
                navController
            ) // creating timer task
        //Set scheduler
        timer.schedule(welcomeTimerTask, meetingTime)
    }
}