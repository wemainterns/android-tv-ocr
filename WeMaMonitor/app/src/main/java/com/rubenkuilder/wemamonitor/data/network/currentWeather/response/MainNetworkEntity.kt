package com.rubenkuilder.wemamonitor.data.network.currentWeather.response

data class MainNetworkEntity(
    val feelsLike: Double,
    val humidity: Int,
    val pressure: Int,
    val temp: Double,
    val tempMax: Double,
    val tempMin: Double
)