package com.rubenkuilder.wemamonitor.data.network.envFeed

import androidx.lifecycle.LiveData
import com.rubenkuilder.wemamonitor.data.network.envFeed.response.EnvFeedNetworkEntity

interface EnvFeedNetworkDataSource {
    val downloadedEnvFeedNetworkEntity: LiveData<List<EnvFeedNetworkEntity>>

    suspend fun fetchEnvFeed()
}