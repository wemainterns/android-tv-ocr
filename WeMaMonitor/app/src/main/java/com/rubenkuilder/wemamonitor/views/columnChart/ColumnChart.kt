package com.rubenkuilder.wemamonitor.views.columnChart

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.View

class ColumnChart @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
    // Display info
    private var labelTopMargin: Int = 30
    private var labelTextSize: Float? = null
    private var axisTextSize: Float? = null
    private var axisMargin: Float? = null


    // Data
    private var data: ColumnData? = null

    // Graphics
    private val labelTextPaint = Paint()
    private val axisTextPaint = Paint()
    private val axisPaint = Paint()

    private var axisTextPaintFm: Paint.FontMetrics = axisTextPaint.fontMetrics
    private var axisTextPaintHeight = axisTextPaintFm.descent - axisTextPaintFm.ascent

    init {
        labelTextPaint.apply {
            isAntiAlias = true
            color = Color.BLACK
            textAlign = Paint.Align.CENTER
        }
        axisTextPaint.apply {
            isAntiAlias = true
            color = Color.LTGRAY
        }
        axisPaint.apply {
            isAntiAlias = true
            color = Color.LTGRAY
        }
    }

    /**
     * Populates the data object and sets up the view based off the new data
     *
     * @param data the new set of data to be represented by the column chart
     */
    fun setData(data: ColumnData) {
        this.data = data
        invalidate()
    }

    /**
     * Sets the text sizes
     */
    private fun setTextSizes() {
        axisTextSize = height / 20f
        labelTextSize = width / 60f

        axisMargin = axisTextPaint.measureText(data?.maxValue.toString()) + 10

        axisTextPaint.textSize = axisTextSize!!
        Log.i("ColumnChart", "${axisTextPaint.measureText(data?.maxValue.toString()).toInt()}")
        labelTextPaint.textSize = labelTextSize!!
    }

    /**
     * Calculates and sets the dimensions and positions of the columnBars in the column chart
     */
    private fun setColumnBarDimensions() {
        var lastEndWidth = axisMargin!!
        var lastLabelHeight = labelTextSize!!
        var totalColumns = data?.columnBars?.size
        data?.columnBars?.forEach {
            // starting width is the location of the last ending width
            it.value.startWidth = lastEndWidth

            // end width is determined by deviding the total view width by the total amount of columns
            it.value.endWidth = (lastEndWidth + ((width - axisMargin!!) / totalColumns!!))
            it.value.height = (((height - labelTopMargin!!) - ((height - labelTopMargin!!) * (it.value.value / data?.maxValue!!))) + (axisTextPaintHeight / 2)).toFloat()

            lastEndWidth = it.value.endWidth
        }
    }

    /**
     * Calculates and sets the positions of the labels in the column chart
     */
    private fun setVerticalLabelLocation(key: String, lastLabelHeight: Float, totalColumns: Int): Float {
        val labelLineSpacing: Float = (labelTextSize!! * 1.5).toFloat()
        data?.columnBars?.get(key)?.let {
            it.labelLocation.y = lastLabelHeight
            it.labelLocation.x = (width - labelTopMargin!!).toFloat()
        }

        // return the space the next item should be placed under the current item
        return labelLineSpacing
    }

    /**
     * Re-calculates graphic sizes if size of view is changed
     */
    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        // setTextSizes() has to be called first, otherwise the label line spacing won't be set correctly
        setTextSizes()
        setColumnBarDimensions()
    }

    /**
     * Draws the view onto the screen
     *
     * @param canvas canvas object to be used to draw
     */
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        data?.columnBars?.let { bars ->
            bars.forEach {
                canvas?.drawRect(
                    it.value.startWidth,
                    it.value.height,
                    it.value.endWidth,
                    (height - labelTopMargin!!).toFloat(),
                    it.value.paint
                )
                canvas?.drawText(
                    it.value.name,
                    (it.value.startWidth + it.value.endWidth) / 2,
                    (height - 5).toFloat(),
                    labelTextPaint
                )
            }

            // Draw chart axis
            canvas?.drawText(
                data?.maxValue.toString(),
                0f,
                axisTextPaintHeight,
                axisTextPaint
            )
            canvas?.drawText(
                "0.0",
                0f,
                ((height - labelTopMargin!!) + (axisTextPaintHeight / 2)),
                axisTextPaint
            )
            canvas?.drawRect(axisMargin!!, 0f, axisMargin!! + 3, (height - labelTopMargin!!).toFloat(), axisPaint)
        }
    }
}