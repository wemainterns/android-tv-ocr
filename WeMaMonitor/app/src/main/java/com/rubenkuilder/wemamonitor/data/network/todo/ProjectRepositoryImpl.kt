package com.rubenkuilder.wemamonitor.data.network.todo

import androidx.lifecycle.MutableLiveData
import javax.inject.Inject

/**
 * Singleton pattern
 */
class ProjectRepositoryImpl
@Inject
constructor(): ProjectRepository {
    private var dataSet = ArrayList<Project>()
    private var doingData = ArrayList<Project>()
    private var testingData = ArrayList<Project>()
    private var doneData = ArrayList<Project>()

    /**
     * Pretend to get doing data from a webservice or online source
     */
    override fun getDoingData(): MutableLiveData<ArrayList<Project>> {
        if(dataSet.size == 0) {
            setProjects()
        }

        val data = MutableLiveData<ArrayList<Project>>()
        data.value = doingData

        return data
    }

    /**
     * Pretend to get testing data from a webservice or online source
     */
    override fun getTestingData(): MutableLiveData<ArrayList<Project>> {
        if(dataSet.size == 0) {
            setProjects()
        }

        val data = MutableLiveData<ArrayList<Project>>()
        data.value = testingData

        return data
    }

    /**
     * Pretend to get done data from a webservice or online source
     */
    override fun getDoneData(): MutableLiveData<ArrayList<Project>> {
        if(dataSet.size == 0) {
            setProjects()
        }

        val data = MutableLiveData<ArrayList<Project>>()
        data.value = doneData

        return data
    }

    /**
     * Add projects to dataSet list
     */
    override fun setProjects() {
        dataSet.add(Project("Project 1", 0, "https://www.pdn.cam.ac.uk/teaching/teaching_images/iTunes-U-1.4-for-iOS-app-icon-small.png/image_preview"))
        dataSet.add(Project("Project 2", 1, "https://www.pdn.cam.ac.uk/teaching/teaching_images/iTunes-U-1.4-for-iOS-app-icon-small.png/image_preview"))
        dataSet.add(Project("Project 3", 2, "https://www.pdn.cam.ac.uk/teaching/teaching_images/iTunes-U-1.4-for-iOS-app-icon-small.png/image_preview"))
        dataSet.add(Project("Test", 2, "https://www.pdn.cam.ac.uk/teaching/teaching_images/iTunes-U-1.4-for-iOS-app-icon-small.png/image_preview"))
        dataSet.add(Project("WeMa", 1, "https://www.pdn.cam.ac.uk/teaching/teaching_images/iTunes-U-1.4-for-iOS-app-icon-small.png/image_preview"))
        dataSet.add(Project("Hondje", 2, "https://www.pdn.cam.ac.uk/teaching/teaching_images/iTunes-U-1.4-for-iOS-app-icon-small.png/image_preview"))

        validateProjects()
    }

    /**
     * Put each project in the correct List
     */
    override fun validateProjects() {
        for(project in dataSet) {
            when(project.status) {
                0 -> doingData.add(project)
                1 -> testingData.add(project)
                2 -> doneData.add(project)
            }
        }
    }
}