package com.rubenkuilder.wemamonitor.views.fallingImages

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.Log
import com.rubenkuilder.wemamonitor.R
import kotlin.random.Random


class Image(
    var x: Float,
    var y: Float,
    var width: Int,
    var imageDrawable: Drawable,
    mContext: Context
) {
    var myBall: Bitmap = (imageDrawable as BitmapDrawable).bitmap
//    var speed: Int = Random.nextInt(50, 200)
    var speed: Int = Random.nextInt(50, 200)

    init {
        myBall = resizeBitmap(myBall, width)
    }

    // Method to resize a bitmap programmatically
    /**
     * Method to resize a bitmap programmatically
     *
     * @param bitmap the source bitmap
     * @param width the new bitmap's desired width
     * @param height the new bitmap's desired height
     * @return the new scaled bitmap or the source bitmap if no scaling is required
     * @throws IllegalAccessException : if width is <= 0, or height is <= 0
     */
    private fun resizeBitmap(bitmap: Bitmap, width: Int): Bitmap{
        val height = (bitmap.height * (width.toDouble() / bitmap.width.toDouble())).toInt()

        return Bitmap.createScaledBitmap(
            bitmap,
            width,
            height,
            false
        )
    }
}