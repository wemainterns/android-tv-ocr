package com.rubenkuilder.wemamonitor.ui.employeeOfTheMonth

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.rubenkuilder.wemamonitor.R
import com.rubenkuilder.wemamonitor.utilities.SCREEN_NAV_IN_MS
import com.rubenkuilder.wemamonitor.databinding.FragmentEmployeeOfTheMonthBinding
import dagger.hilt.android.AndroidEntryPoint

/**
 * Fragment which shows the employee of the month
 */
@AndroidEntryPoint
class EmployeeOfTheMonthFragment : Fragment() {
    private lateinit var binding: FragmentEmployeeOfTheMonthBinding
    private lateinit var viewModel: EmployeeOfTheMonthViewModel
    private lateinit var navController: NavController

    private val mainHandler = Handler()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.i("Test", "---------------------")
        Log.i("Test - EmployeeOfTheMonthFragment", "Fragment view created")
        val fm: FragmentManager? = fragmentManager
        Log.i("Test - EmployeeOfTheMonthFragment", "backStackEntryCount: ${fm!!.backStackEntryCount}")

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_employee_of_the_month, container, false)

        // Initialize variables
        viewModel = ViewModelProvider(this).get(EmployeeOfTheMonthViewModel::class.java)
        navController = findNavController()

        viewModel.employee.observe(viewLifecycleOwner, Observer { employee ->
            if(employee == null) {
                navController.navigate(R.id.action_employeeOfTheMonthFragment_to_clientQuoteFragment)
            } else {
                Glide.with(this).load(employee.imageURL).into(binding.employeeImage)
                binding.employeeName.text = employee.name

                autoNavigateNext()
            }
        })

        return binding.root
    }

    /**
     * Timer which handles navigation to ServerStatus fragment
     */
    private fun autoNavigateNext() {
        mainHandler.postDelayed({
                //Keep navigating
                Log.i("Test - EmployeeOfTheMonthFragment", "Keep navigating")
                Log.i("Test - EmployeeOfTheMonthFragment", "Navigating from EmployeeOfTheMonth to ClientQuoteFragment")
                mainHandler.removeCallbacksAndMessages(null)
                navController.navigate(R.id.action_employeeOfTheMonthFragment_to_clientQuoteFragment)
        }, SCREEN_NAV_IN_MS)
    }

    override fun onPause() {
        super.onPause()
        Log.i("Test - EmployeeOfTheMonth", "onDestroyView")
        mainHandler.removeCallbacksAndMessages(null)
    }
}