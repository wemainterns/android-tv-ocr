package com.rubenkuilder.wemamonitor.data.network.currentWeather.response

data class WeatherNetworkEntity(
    val description: String,
    val icon: String
)