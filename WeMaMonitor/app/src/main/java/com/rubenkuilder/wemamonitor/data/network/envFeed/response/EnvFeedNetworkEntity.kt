package com.rubenkuilder.wemamonitor.data.network.envFeed.response

import com.google.gson.annotations.SerializedName

data class EnvFeedNetworkEntity(
    @SerializedName("_id")
    val product_id: OIDNetworkEntity,
    val product: String,
    val employee: String,
    val from_env: List<String>,
    val to_env: List<String>,
    val date: String,
    val changelog: String
)