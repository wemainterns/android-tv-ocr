package com.rubenkuilder.wemamonitor.data.domain.currentWeather

data class CurrentWeather(
    var cityID: Int,
    var weatherDesc: String,
    var weatherIcon: String,
    val feelsLike: Double,
    val humidity: Int,
    val pressure: Int,
    val temp: Double,
    val tempMax: Double,
    val tempMin: Double
)