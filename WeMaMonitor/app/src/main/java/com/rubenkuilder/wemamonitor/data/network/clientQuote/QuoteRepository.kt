package com.rubenkuilder.wemamonitor.data.network.clientQuote

import androidx.lifecycle.MutableLiveData

interface QuoteRepository {
    fun getQuote(): MutableLiveData<Quote>
    fun setQuotes()
}