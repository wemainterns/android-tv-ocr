package com.rubenkuilder.wemamonitor.data.network.envFeed

import com.rubenkuilder.wemamonitor.data.network.ConnectivityInterceptor
import com.rubenkuilder.wemamonitor.data.network.envFeed.response.EnvFeedNetworkEntity
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

//TODO: echte data uit echte API halen
interface EnvFeedApiService {
    @GET("S6SG")
    suspend fun getEnvFeed(
    ): List<EnvFeedNetworkEntity>

    /**
     * Make connection with the API URL
     */
    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor
        ): EnvFeedApiService {
            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(connectivityInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://jsonkeeper.com/b/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(EnvFeedApiService::class.java)
        }
    }
}