package com.rubenkuilder.wemamonitor.data.domain.envFeed

data class EnvFeed(
    val product_id: String,
    val product: String,
    val employee: String,
    val from_env: List<String>,
    val to_env: List<String>,
    val date: String,
    val changelog: String
)