package com.rubenkuilder.wemamonitor.data.network.currentWeather.response

import com.google.gson.annotations.SerializedName

data class CurrentWeatherNetworkEntity(
    @SerializedName("id")
    val cityID: Int,
    val main: MainNetworkEntity,
    val weather: List<WeatherNetworkEntity>
)