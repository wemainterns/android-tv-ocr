package com.rubenkuilder.wemamonitor.views.columnChart

import android.graphics.Paint
import android.graphics.PointF

data class ColumnBar(
    val name: String,
    var value: Double,
    var height: Float,
    var startWidth: Float,
    var endWidth: Float,
    var labelLocation: PointF,
    val paint: Paint
)