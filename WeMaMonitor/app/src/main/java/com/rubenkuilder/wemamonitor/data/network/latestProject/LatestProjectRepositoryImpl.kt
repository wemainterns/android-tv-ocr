package com.rubenkuilder.wemamonitor.data.network.latestProject

import androidx.lifecycle.MutableLiveData
import javax.inject.Inject

class LatestProjectRepositoryImpl
@Inject
constructor() : LatestProjectRepository {
    private var latestProject: LatestProject? = null

    override fun getLatestProject(): MutableLiveData<LatestProject> {
        setLatestProject()

        val data = MutableLiveData<LatestProject>()
        data.value = latestProject

        return data
    }

    /**
     * Add latest project
     */
    override fun setLatestProject() {
        latestProject = LatestProject(
            "Oegema",
            "https://www.pdn.cam.ac.uk/teaching/teaching_images/iTunes-U-1.4-for-iOS-app-icon-small.png/image_preview",
            "Klaasje",
            "https://developer.android.com/images/training/testing/espresso.png"
        )
    }
}