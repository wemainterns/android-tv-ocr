package com.rubenkuilder.wemamonitor.data.network.employeeOfTheMonth

import androidx.lifecycle.MutableLiveData

interface EmployeeRepository {
    fun getEmployee(): MutableLiveData<Employee>
    fun setEmployee()
}