package com.rubenkuilder.wemamonitor.ui.envFeed

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.rubenkuilder.wemamonitor.data.repository.envFeed.EnvFeedRepository
import com.rubenkuilder.wemamonitor.internal.lazyDeferred

class EnvFeedViewModel
@ViewModelInject
constructor(
    private val EnvFeedRepositoryImpl: EnvFeedRepository
): ViewModel() {
    val envFeed by lazyDeferred {
        EnvFeedRepositoryImpl.getEnvFeed()
    }
}