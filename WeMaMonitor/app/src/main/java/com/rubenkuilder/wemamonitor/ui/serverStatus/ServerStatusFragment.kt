package com.rubenkuilder.wemamonitor.ui.serverStatus

import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.rubenkuilder.wemamonitor.R
import com.rubenkuilder.wemamonitor.utilities.SCREEN_NAV_IN_MS
import com.rubenkuilder.wemamonitor.databinding.FragmentServerStatusBinding


/**
 * Fragment which shows the status of servers
 */
class ServerStatusFragment: Fragment() {
    private lateinit var binding: FragmentServerStatusBinding
    private lateinit var serverStatusFragmentViewModel: ServerStatusViewModel
    private lateinit var navController: NavController

    private val mainHandler = Handler()
    private var readyToNavigate = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.i("Test", "---------------------")
        Log.i("Test - serverStatusFragment", "Fragment view created")
        val fm: FragmentManager? = fragmentManager
        Log.i("Test - serverStatusFragment", "backStackEntryCount: ${fm!!.backStackEntryCount}")

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_server_status, container, false)

        // Initialize variables
        serverStatusFragmentViewModel = ViewModelProvider(this).get(ServerStatusViewModel::class.java)
        navController = findNavController()

        /**
         * Start ping handler loop
         */
        serverStatusFragmentViewModel.pingHandler()


        /**
         * Observe the status of the servers
          */
        serverStatusFragmentViewModel.iAppLiveStatus.observe(viewLifecycleOwner, Observer { status ->
            Log.i("Test - ServerStatusFragment - liveObserver", "$status")
            when(status) {
                true -> {
                    binding.iAppLiveStatus.text = "Online"
                    val shape = binding.iAppLiveStatus.background as GradientDrawable
                    shape.setColor(resources.getColor(R.color.green))
                }
                false -> {
                    binding.iAppLiveStatus.text = "Offline"
                    val shape = binding.iAppLiveStatus.background as GradientDrawable
                    shape.setColor(resources.getColor(R.color.red))
                }
            }
        })

        serverStatusFragmentViewModel.iAppBetaStatus.observe(viewLifecycleOwner, Observer { status ->
            Log.i("Test - ServerStatusFragment - betaObserver", "$status")
            when(status) {
                true -> {
                    binding.iAppBetaStatus.text = "Online"
                    val shape = binding.iAppBetaStatus.background as GradientDrawable
                    shape.setColor(resources.getColor(R.color.green))
                }
                false -> {
                    binding.iAppBetaStatus.text = "Offline"
                    val shape = binding.iAppBetaStatus.background as GradientDrawable
                    shape.setColor(resources.getColor(R.color.red))
                }
            }
        })

        serverStatusFragmentViewModel.iAppDevStatus.observe(viewLifecycleOwner, Observer { status ->
            Log.i("Test - ServerStatusFragment - devObserver", "$status")
            when(status) {
                true -> {
                    binding.iAppDevStatus.text = "Online"
                    val shape = binding.iAppDevStatus.background as GradientDrawable
                    shape.setColor(resources.getColor(R.color.green))
                }
                false -> {
                    binding.iAppDevStatus.text = "Offline"
                    val shape = binding.iAppDevStatus.background as GradientDrawable
                    shape.setColor(resources.getColor(R.color.red))
                }
            }
        })

        autoNavigateNext()

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        readyToNavigate = true

        Log.i("Test - ServerStatusFragment", "onResume")
    }

    /**
     * Timer which handles navigation to the to-do fragment
     */
    private fun autoNavigateNext() {
        Log.i("Test - ServerStatusFragment", "readyToNavigate = $readyToNavigate")

        mainHandler.postDelayed({
            //Keep navigating
            Log.i("Test - serverstatusfragment", "Keep navigating")
            Log.i("Test - serverStatusFragment", "Navigating from serverStatusFragment to TodoFragment")
            mainHandler.removeCallbacksAndMessages(null)
            if(readyToNavigate) {
                navController.navigate(R.id.action_serverStatusFragment_to_todoFragment)
            }
        }, SCREEN_NAV_IN_MS)
    }

    override fun onPause() {
        super.onPause()
        Log.i("Test - serverstatusfragment", "onDestroyView")
        mainHandler.removeCallbacksAndMessages(null)
    }
}