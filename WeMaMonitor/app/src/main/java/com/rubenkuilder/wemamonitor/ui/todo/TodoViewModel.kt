package com.rubenkuilder.wemamonitor.ui.todo

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rubenkuilder.wemamonitor.data.network.todo.Project
import com.rubenkuilder.wemamonitor.data.network.todo.ProjectRepositoryImpl

class TodoViewModel
@ViewModelInject
constructor(
    ProjectRepositoryImpl: ProjectRepositoryImpl
): ViewModel() {
    private var projects = ArrayList<Project>()

    private var _projectsDoing = MutableLiveData<ArrayList<Project>>()
    val projectsDoing: LiveData<ArrayList<Project>>
        get() = _projectsDoing

    private var _projectsTesting = MutableLiveData<ArrayList<Project>>()
    val projectsTesting: LiveData<ArrayList<Project>>
        get() = _projectsTesting

    private var _projectsDone = MutableLiveData<ArrayList<Project>>()
    val projectsDone: LiveData<ArrayList<Project>>
        get() = _projectsDone

    init {
        Log.i("Test - TodoViewModel", "init & $projects")
        _projectsDoing = ProjectRepositoryImpl.getDoingData()
        _projectsTesting = ProjectRepositoryImpl.getTestingData()
        _projectsDone = ProjectRepositoryImpl.getDoneData()
    }
}