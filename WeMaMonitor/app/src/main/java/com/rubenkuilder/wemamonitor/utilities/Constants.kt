package com.rubenkuilder.wemamonitor.utilities

//TODO: It's probably better to put the constants in the appropriate file

//JSON URLs
const val IAPP_DEV_URL = "iapp-dev.cloud"
const val IAPP_BETA_URL = "iapp-beta.cloud"
const val IAPP_LIVE_URL = "iapp.cloud"

// Pinging
const val PING_INTERVAL_IN_MS: Long = 60000 //1 minute

// Screen navigation
const val SCREEN_NAV_IN_MS: Long = 10000 //10 seconds

// Testing times
const val TEST_START_HOUR = 10
const val TEST_START_MINUTE = 38
const val TEST_END_HOUR = 10
const val TEST_END_MINUTE = 39
    // Testing flash interval
    const val TEST_TIMER_FLASH_IN_MS: Long = 1000

//OrderReminder
const val ORDERREMINDER_START_HOUR = 10
const val ORDERREMINDER_START_MINUTE = 40
const val ORDERREMINDER_END_HOUR = 10
const val ORDERREMINDER_END_MINUTE = 41

// Break times
// Break 1
const val BREAK1_START_HOUR = 10
const val BREAK1_START_MINUTE = 42
const val BREAK1_END_HOUR = 10
const val BREAK1_END_MINUTE = 43
// Break 2
const val BREAK2_START_HOUR = 10
const val BREAK2_START_MINUTE = 44
const val BREAK2_END_HOUR = 10
const val BREAK2_END_MINUTE = 45
// Break 3
const val BREAK3_START_HOUR = 10
const val BREAK3_START_MINUTE = 46
const val BREAK3_END_HOUR = 10
const val BREAK3_END_MINUTE = 47

// Client meeting times
const val SHOW_X_MIN_BEFORE_CLIENT_MEETING: Long = 1
const val END_X_MIN_AFTER_CLIENT_MEETING: Long = 1