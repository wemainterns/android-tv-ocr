package com.rubenkuilder.wemamonitor.data.network.latestProject

import androidx.lifecycle.MutableLiveData

interface LatestProjectRepository {
    fun getLatestProject(): MutableLiveData<LatestProject>
    fun setLatestProject()
}