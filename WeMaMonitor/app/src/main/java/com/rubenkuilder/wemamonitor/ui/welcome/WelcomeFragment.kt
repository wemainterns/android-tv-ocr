package com.rubenkuilder.wemamonitor.ui.welcome

import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.rubenkuilder.wemamonitor.*
import com.rubenkuilder.wemamonitor.databinding.FragmentWelcomeBinding
import com.rubenkuilder.wemamonitor.utilities.END_X_MIN_AFTER_CLIENT_MEETING
import com.rubenkuilder.wemamonitor.utilities.SHOW_X_MIN_BEFORE_CLIENT_MEETING
import dagger.hilt.android.AndroidEntryPoint

/**
 * Fragment which welcomes a client in the office
 */
@AndroidEntryPoint
class WelcomeFragment : Fragment() {
    private lateinit var binding: FragmentWelcomeBinding
    private lateinit var viewModel: WelcomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.i("Test", "---------------------")
        Log.i("Test - WelcomeFragment", "Fragment view created")
        val fm: FragmentManager? = fragmentManager
        Log.i("Test - WelcomeFragment", "backStackEntryCount: ${fm!!.backStackEntryCount}")

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_welcome, container, false)

        viewModel = ViewModelProvider(this).get(WelcomeViewModel::class.java)

        viewModel.meeting.observe(viewLifecycleOwner, Observer { meeting ->
            val clientName = meeting.clientName
            Log.i("Test - WelcomeFragment", "$clientName")

            binding.clientName.text = clientName
        })

        autoNavigateNext()

        return binding.root
    }

    /**
     * Timer which handles navigation to ServerStatus fragment
     */
    private fun autoNavigateNext() {
        val mainHandler = Handler()
        mainHandler.postDelayed({
            requireActivity().onBackPressed()
        }, (SHOW_X_MIN_BEFORE_CLIENT_MEETING + END_X_MIN_AFTER_CLIENT_MEETING) * (1000 * 60))
    }
}