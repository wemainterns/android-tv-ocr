package com.rubenkuilder.wemamonitor.ui.latestProject

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rubenkuilder.wemamonitor.data.network.latestProject.LatestProject
import com.rubenkuilder.wemamonitor.data.network.latestProject.LatestProjectRepository
import com.rubenkuilder.wemamonitor.data.network.latestProject.LatestProjectRepositoryImpl

class LatestProjectViewModel
@ViewModelInject
constructor(
    LatestProjectRepositoryImpl: LatestProjectRepository
): ViewModel() {
    private var _latestProject = MutableLiveData<LatestProject>()
    val latestProject: LiveData<LatestProject>
        get() = _latestProject

    init {
        _latestProject = LatestProjectRepositoryImpl.getLatestProject()
    }
}