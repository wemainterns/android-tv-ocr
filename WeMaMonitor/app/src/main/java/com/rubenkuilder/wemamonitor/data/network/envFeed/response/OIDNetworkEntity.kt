package com.rubenkuilder.wemamonitor.data.network.envFeed.response

import com.google.gson.annotations.SerializedName

data class OIDNetworkEntity (
    @SerializedName("\$oid")
    val oid: String
)