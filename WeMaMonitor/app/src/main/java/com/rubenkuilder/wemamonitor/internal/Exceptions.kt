package com.rubenkuilder.wemamonitor.internal

import java.io.IOException

class NoConnectivityException: IOException()