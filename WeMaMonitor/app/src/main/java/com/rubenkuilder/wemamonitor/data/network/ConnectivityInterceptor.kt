package com.rubenkuilder.wemamonitor.data.network

import okhttp3.Interceptor

interface ConnectivityInterceptor: Interceptor