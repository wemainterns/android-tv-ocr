package com.rubenkuilder.wemamonitor.data.network.welcome

import androidx.lifecycle.MutableLiveData
import javax.inject.Inject

class WelcomeRepositoryImpl
@Inject
constructor(): WelcomeRepository {
    private var meeting: Meeting? = null

    /**
     * Pretend to get data from a webservice or online source
     */
    override fun getMeeting(): MutableLiveData<Meeting> {
        setMeeting()

        val data = MutableLiveData<Meeting>()
        data.value = meeting

        return data
    }

    /**
     * Add a meeting
     */
    override fun setMeeting() {
        meeting = Meeting("Koen", "16-10-2020 16:41", "https://developer.android.com/images/training/testing/espresso.png")
    }
}