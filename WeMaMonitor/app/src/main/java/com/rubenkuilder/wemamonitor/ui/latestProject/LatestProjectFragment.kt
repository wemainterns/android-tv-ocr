package com.rubenkuilder.wemamonitor.ui.latestProject

import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.rubenkuilder.wemamonitor.R
import com.rubenkuilder.wemamonitor.databinding.FragmentLatestProjectBinding
import com.rubenkuilder.wemamonitor.utilities.SCREEN_NAV_IN_MS
import dagger.hilt.android.AndroidEntryPoint

/**
 * Fragment which shows the latest finished project
 */
@AndroidEntryPoint
class LatestProjectFragment : Fragment() {
    private lateinit var binding: FragmentLatestProjectBinding
    private lateinit var viewModel: LatestProjectViewModel
    private lateinit var navController: NavController

    private val mainHandler = Handler()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_latest_project, container, false)

        viewModel = ViewModelProvider(this).get(LatestProjectViewModel::class.java)
        navController = findNavController()

        viewModel.latestProject.observe(viewLifecycleOwner, Observer { project ->
            Log.i("Test - LatestProjectFragment", "$project")
            binding.projectName.text = project.projectName
            Glide.with(requireContext()).load(project.projectImageURL).into(binding.projectImage)
        })

        autoNavigateNext()

        return binding.root
    }

    /**
     * Timer which handles navigation to ServerStatus fragment
     */
    private fun autoNavigateNext() {
        mainHandler.postDelayed({
            //Keep navigating
            Log.i("Test - LatestProjectFragment", "Keep navigating")
            Log.i("Test - LatestProjectFragment", "Navigating from LatestProject to ServerStatusFragment")
            mainHandler.removeCallbacksAndMessages(null)
            navController.navigate(R.id.action_latestProjectFragment_to_currentWeatherFragment)
        }, SCREEN_NAV_IN_MS)
    }

    override fun onPause() {
        super.onPause()
        Log.i("Test - LatestProjectFragment", "onDestroyView")
        mainHandler.removeCallbacksAndMessages(null)
    }
}