package com.rubenkuilder.wemamonitor.data.network.clientQuote

data class Quote(
    val name: String,
    val imageURL: String,
    val quote: String
) {
    override fun toString(): String {
        return "\"$quote\" - $name"
    }
}