package com.rubenkuilder.wemamonitor.data.network.employeeOfTheMonth

data class Employee(val name: String, val imageURL: String) {
    override fun toString(): String {
        return name
    }
}