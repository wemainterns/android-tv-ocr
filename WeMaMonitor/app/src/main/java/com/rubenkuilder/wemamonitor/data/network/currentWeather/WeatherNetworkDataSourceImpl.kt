package com.rubenkuilder.wemamonitor.data.network.currentWeather

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rubenkuilder.wemamonitor.data.network.currentWeather.response.CurrentWeatherNetworkEntity
import com.rubenkuilder.wemamonitor.internal.NoConnectivityException

class WeatherNetworkDataSourceImpl(
    private val openWeatherApiService: OpenWeatherApiService
) : WeatherNetworkDataSource {
    private val _downloadedCurrentWeather = MutableLiveData<CurrentWeatherNetworkEntity>()
    override val downloadedCurrentWeatherNetworkEntity: LiveData<CurrentWeatherNetworkEntity>
        get() = _downloadedCurrentWeather

    /**
     * Fetch the current weather from the openWeatherApiService
     */
    override suspend fun fetchCurrentWeather(cityID: Int, units: String) {
        try {
            val fetchedCurrentWeather = openWeatherApiService.getCurrentWeather(cityID, units)

            Log.i("Test - WeatherNetworkDataSourceImpl", fetchedCurrentWeather.toString())

            _downloadedCurrentWeather.postValue(fetchedCurrentWeather)
        } catch (e: NoConnectivityException) {
            Log.e("Connectivity", "No internet connection.", e)
        }
    }
}