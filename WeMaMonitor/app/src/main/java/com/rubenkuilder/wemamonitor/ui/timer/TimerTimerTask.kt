package com.rubenkuilder.wemamonitor.ui.timer

import android.util.Log
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import com.rubenkuilder.wemamonitor.R
import com.rubenkuilder.wemamonitor.utilities.Utils.checkMeeting
import com.rubenkuilder.wemamonitor.utilities.Utils.checkTime
import com.rubenkuilder.wemamonitor.utilities.Utils.isBreak
import java.util.*

class TimerTimerTask(private val caller: String, private val navController: NavController) : TimerTask() {
    override fun run() {
        if(checkTime() && !checkMeeting()) {
            Log.i("Test - CheckTimerTask", "checkTime = ${checkTime()}")
            Log.i("Test - CheckTimerTask", "isBreak = ${isBreak()}")

            Log.i("Test - CheckTimerTask", "caller = $caller")

            val navOptions = NavOptions.Builder().setPopUpTo(R.id.TimerFragment, true).build()
            navController.navigate(R.id.TimerFragment, null, navOptions)
        }
    }
}