package com.rubenkuilder.wemamonitor.ui.employeeOfTheMonth

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rubenkuilder.wemamonitor.data.network.employeeOfTheMonth.Employee
import com.rubenkuilder.wemamonitor.data.network.employeeOfTheMonth.EmployeeRepository

class EmployeeOfTheMonthViewModel
@ViewModelInject
constructor(
    EmployeeRepositoryImpl: EmployeeRepository
): ViewModel() {
    private var _employee = MutableLiveData<Employee>()
    val employee: LiveData<Employee>
        get() = _employee

    init {
        _employee = EmployeeRepositoryImpl.getEmployee()
    }
}