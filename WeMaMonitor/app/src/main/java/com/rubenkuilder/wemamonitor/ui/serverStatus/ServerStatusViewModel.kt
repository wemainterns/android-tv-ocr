package com.rubenkuilder.wemamonitor.ui.serverStatus

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rubenkuilder.wemamonitor.utilities.IAPP_BETA_URL
import com.rubenkuilder.wemamonitor.utilities.IAPP_DEV_URL
import com.rubenkuilder.wemamonitor.utilities.IAPP_LIVE_URL
import kotlinx.coroutines.*
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketAddress
import kotlin.coroutines.CoroutineContext

class ServerStatusViewModel : ViewModel() {
    private val _iAppLiveStatus = MutableLiveData<Boolean>()
    val iAppLiveStatus: LiveData<Boolean>
        get() = _iAppLiveStatus

    private val _iAppBetaStatus = MutableLiveData<Boolean>()
    val iAppBetaStatus: LiveData<Boolean>
        get() = _iAppBetaStatus

    private val _iAppDevStatus = MutableLiveData<Boolean>()
    val iAppDevStatus: LiveData<Boolean>
        get() = _iAppDevStatus

    private val ioContext: CoroutineContext
        get() = Dispatchers.IO

    fun pingHandler() {
        // Code here
        ping(IAPP_LIVE_URL)
        ping(IAPP_BETA_URL)
        ping(IAPP_DEV_URL)

        Log.i("Test - ServerStatusViewModel - live", "${iAppLiveStatus.value}")
        Log.i("Test - ServerStatusViewModel - beta", "${iAppBetaStatus.value}")
        Log.i("Test - ServerStatusViewModel - dev", "${iAppDevStatus.value}")
    }

    private fun ping(url: String) {
        viewModelScope.launch {
            val deferred: Deferred<Boolean> = async(context = ioContext) {
                getResponseFromUrl(url, 80)
            }

            val resultBool: Boolean = deferred.await()
            Log.i("Test - ServerStatusViewModel - resultBool", "$resultBool")
            when(url) {
                IAPP_LIVE_URL -> _iAppLiveStatus.value = resultBool
                IAPP_BETA_URL -> _iAppBetaStatus.value = resultBool
                IAPP_DEV_URL -> _iAppDevStatus.value = resultBool
            }
        }
    }

    private fun getResponseFromUrl(ip: String, port: Int): Boolean {
        var exists = false

        try {
            val sockaddr: SocketAddress = InetSocketAddress(ip, port)
            // Create an unbound socket
            val sock = Socket()

            // This method will block no more than timeoutMs.
            // If the timeout occurs, SocketTimeoutException is thrown.
            val timeoutMs = 2000 // 2 seconds
            sock.connect(sockaddr, timeoutMs)
            exists = true
        } catch (e: IOException) {
            // Handle exception
        }

        Log.i("Test - ServerStatusViewModel - exists", "$exists")

        return exists
    }
}