package com.rubenkuilder.wemamonitor.di

import android.content.Context
import com.rubenkuilder.wemamonitor.data.db.CurrentWeatherDao
import com.rubenkuilder.wemamonitor.data.db.currentWeather.CurrentWeatherDBMapper
import com.rubenkuilder.wemamonitor.data.db.ForecastDatabase
import com.rubenkuilder.wemamonitor.data.db.envFeed.EnvFeedDBMapper
import com.rubenkuilder.wemamonitor.data.network.ConnectivityInterceptor
import com.rubenkuilder.wemamonitor.data.network.ConnectivityInterceptorImpl
import com.rubenkuilder.wemamonitor.data.network.currentWeather.CurrentWeatherNetworkMapper
import com.rubenkuilder.wemamonitor.data.network.clientQuote.QuoteRepository
import com.rubenkuilder.wemamonitor.data.network.clientQuote.QuoteRepositoryImpl
import com.rubenkuilder.wemamonitor.data.network.currentWeather.OpenWeatherApiService
import com.rubenkuilder.wemamonitor.data.network.currentWeather.WeatherNetworkDataSource
import com.rubenkuilder.wemamonitor.data.network.currentWeather.WeatherNetworkDataSourceImpl
import com.rubenkuilder.wemamonitor.data.network.employeeOfTheMonth.EmployeeRepository
import com.rubenkuilder.wemamonitor.data.network.employeeOfTheMonth.EmployeeRepositoryImpl
import com.rubenkuilder.wemamonitor.data.network.envFeed.EnvFeedApiService
import com.rubenkuilder.wemamonitor.data.network.envFeed.EnvFeedNetworkDataSource
import com.rubenkuilder.wemamonitor.data.network.envFeed.EnvFeedNetworkDataSourceImpl
import com.rubenkuilder.wemamonitor.data.network.envFeed.EnvFeedNetworkMapper
import com.rubenkuilder.wemamonitor.data.network.latestProject.LatestProjectRepository
import com.rubenkuilder.wemamonitor.data.network.latestProject.LatestProjectRepositoryImpl
import com.rubenkuilder.wemamonitor.data.network.todo.ProjectRepository
import com.rubenkuilder.wemamonitor.data.network.todo.ProjectRepositoryImpl
import com.rubenkuilder.wemamonitor.data.network.welcome.WelcomeRepository
import com.rubenkuilder.wemamonitor.data.network.welcome.WelcomeRepositoryImpl
import com.rubenkuilder.wemamonitor.data.repository.currentWeather.CurrentWeatherRepository
import com.rubenkuilder.wemamonitor.data.repository.currentWeather.CurrentWeatherRepositoryImpl
import com.rubenkuilder.wemamonitor.data.repository.envFeed.EnvFeedRepository
import com.rubenkuilder.wemamonitor.data.repository.envFeed.EnvFeedRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

/**
 * This is where dependency injection happens :)
 */
@Module
@InstallIn(ApplicationComponent::class)
abstract class RepositoryModule {
    @Binds
    abstract fun bindQuoteRepository(
        QuoteRepositoryImpl: QuoteRepositoryImpl
    ): QuoteRepository

    @Binds
    abstract fun bindEmployeeRepository(
        EmployeeRepositoryImpl: EmployeeRepositoryImpl
    ): EmployeeRepository

    @Binds
    abstract fun bindLatestProjectRepository(
        LatestProjectRepositoryImpl: LatestProjectRepositoryImpl
    ): LatestProjectRepository

    @Binds
    abstract fun bindProjectRepository(
        ProjectRepositoryImpl: ProjectRepositoryImpl
    ): ProjectRepository

    @Binds
    abstract fun bindWelcomeRepository(
        WelcomeRepositoryImpl: WelcomeRepositoryImpl
    ): WelcomeRepository
}

@Module
@InstallIn(ApplicationComponent::class)
internal object MainRepositoryModule {
    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext context: Context): ForecastDatabase {
        return ForecastDatabase(context)
    }

    @Singleton
    @Provides
    fun provideCurrentWeatherDao(forecastDatabase: ForecastDatabase): CurrentWeatherDao {
        return forecastDatabase.currentWeatherDao()
    }

    @Singleton
    @Provides
    fun provideConnectivityInterceptor(@ApplicationContext context: Context): ConnectivityInterceptor {
        return ConnectivityInterceptorImpl(context)
    }

    @Singleton
    @Provides
    fun provideOpenWeatherApiService(connectivityInterceptor: ConnectivityInterceptor): OpenWeatherApiService {
        return OpenWeatherApiService(connectivityInterceptor)
    }

    @Singleton
    @Provides
    fun provideWeatherNetworkDataSource(openWeatherApiService: OpenWeatherApiService): WeatherNetworkDataSource {
        return WeatherNetworkDataSourceImpl(openWeatherApiService)
    }

    @Singleton
    @Provides
    fun provideForecastRepository(
        currentWeatherDao: CurrentWeatherDao,
        weatherNetworkDataSource: WeatherNetworkDataSource,
        currentWeatherDbMapper: CurrentWeatherDBMapper,
        currentWeatherNetworkMapper: CurrentWeatherNetworkMapper
    ): CurrentWeatherRepository {
        return CurrentWeatherRepositoryImpl(
            currentWeatherDao,
            weatherNetworkDataSource,
            currentWeatherDbMapper,
            currentWeatherNetworkMapper
        )
    }

    @Singleton
    @Provides
    fun provideEnvFeedApiService(connectivityInterceptor: ConnectivityInterceptor): EnvFeedApiService {
        return EnvFeedApiService(connectivityInterceptor)
    }

    @Singleton
    @Provides
    fun provideEnvFeedNetworkDataSource(envFeedApiService: EnvFeedApiService): EnvFeedNetworkDataSource {
        return EnvFeedNetworkDataSourceImpl(envFeedApiService)
    }

    @Singleton
    @Provides
    fun provideEnvFeedRepository(
        currentWeatherDao: CurrentWeatherDao,
        envFeedNetworkDataSource: EnvFeedNetworkDataSource,
        envFeedDbMapper: EnvFeedDBMapper,
        envFeedNetworkMapper: EnvFeedNetworkMapper
    ): EnvFeedRepository {
        return EnvFeedRepositoryImpl(
            currentWeatherDao,
            envFeedNetworkDataSource,
            envFeedDbMapper,
            envFeedNetworkMapper
        )
    }
}