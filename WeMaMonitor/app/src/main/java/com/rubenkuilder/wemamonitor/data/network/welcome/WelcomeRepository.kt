package com.rubenkuilder.wemamonitor.data.network.welcome

import androidx.lifecycle.MutableLiveData

interface WelcomeRepository {
    fun getMeeting(): MutableLiveData<Meeting>
    fun setMeeting()
}