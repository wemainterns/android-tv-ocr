package com.rubenkuilder.wemamonitor.ui.currentWeather

import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.rubenkuilder.wemamonitor.R
import com.rubenkuilder.wemamonitor.databinding.FragmentCurrentWeatherBinding
import com.rubenkuilder.wemamonitor.utilities.SCREEN_NAV_IN_MS
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import java.lang.StringBuilder
import kotlin.coroutines.CoroutineContext

/**
 * Fragment which shows the current weather in Hardenberg and Bremen
 */
@AndroidEntryPoint
class CurrentWeatherFragment : Fragment(), CoroutineScope {
    private lateinit var binding: FragmentCurrentWeatherBinding
    private lateinit var viewModel: CurrentWeatherViewModel
    private lateinit var navController: NavController

    private val mainHandler = Handler()

    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_current_weather, container, false)

        viewModel = ViewModelProvider(this).get(CurrentWeatherViewModel::class.java)
        navController = findNavController()

        job = Job()

        bindUI()

        autoNavigateNext()

        return binding.root
    }

    private fun bindUI() = launch {
        val currentWeather = viewModel.weatherHardenberg.await()
        val currentBremen = viewModel.weatherBremen.await()

        currentWeather.observe(viewLifecycleOwner, Observer {
            if(it == null) return@Observer

            Log.i("Test - CurrentWeatherFragment", "$it")
            Log.i("Test - CurrentWeatherFragment", "${it.cityID}")

            binding.hardenbergWeatherTemp.text = getString(R.string.temp, it.temp.toString())

            val sb = StringBuilder()
            sb.append("http://openweathermap.org/img/wn/")
            sb.append(it.weatherIcon)
            sb.append("@2x.png")

            binding.hardenbergWeatherDesc.text = it.weatherDesc!!.capitalize()
            Glide.with(requireContext()).load(sb.toString()).into(binding.hardenbergWeatherSymbol)
        })

        currentBremen.observe(viewLifecycleOwner, Observer {
            if(it == null) return@Observer

            Log.i("Test - CurrentWeatherFragment", "$it")
            Log.i("Test - CurrentWeatherFragment", "${it.cityID}")

            binding.bremenWeatherTemp.text = getString(R.string.temp, it.temp.toString())

            val sb = StringBuilder()
            sb.append("http://openweathermap.org/img/wn/")
            sb.append(it.weatherIcon)
            sb.append("@2x.png")

            binding.bremenWeatherDesc.text = it.weatherDesc!!.capitalize()
            Glide.with(requireContext()).load(sb.toString()).into(binding.bremenWeatherSymbol)
        })
    }

    /**
     * Timer which handles navigation to EnvFeed fragment
     */
    private fun autoNavigateNext() {
        mainHandler.postDelayed({
            //Keep navigating
            mainHandler.removeCallbacksAndMessages(null)
            navController.navigate(R.id.action_currentWeatherFragment_to_envFeedFragment)
        }, SCREEN_NAV_IN_MS)
    }

    override fun onPause() {
        super.onPause()
        mainHandler.removeCallbacksAndMessages(null)
    }

    override fun onDestroy() {
        job.cancel()
        super.onDestroy()
    }
}