package com.rubenkuilder.wemamonitor.data.network.envFeed

import com.rubenkuilder.wemamonitor.data.domain.envFeed.EnvFeed
import com.rubenkuilder.wemamonitor.data.network.envFeed.response.EnvFeedNetworkEntity
import com.rubenkuilder.wemamonitor.data.network.envFeed.response.OIDNetworkEntity
import com.rubenkuilder.wemamonitor.utilities.EntityMapper
import javax.inject.Inject

class EnvFeedNetworkMapper
@Inject
constructor() : EntityMapper<EnvFeedNetworkEntity, EnvFeed> {
    override fun mapFromEntity(entity: EnvFeedNetworkEntity): EnvFeed {
        return EnvFeed(
            product_id = entity.product_id.oid,
            product = entity.product,
            employee = entity.employee,
            from_env = entity.from_env,
            to_env = entity.to_env,
            date = entity.date,
            changelog = entity.changelog
        )
    }

    override fun mapToEntity(domainModel: EnvFeed): EnvFeedNetworkEntity {
        val idEntity = OIDNetworkEntity(domainModel.product_id)

        return EnvFeedNetworkEntity(
            product_id = idEntity,
            product = domainModel.product,
            employee = domainModel.employee,
            from_env = domainModel.from_env,
            to_env = domainModel.to_env,
            date = domainModel.date,
            changelog = domainModel.changelog
        )
    }

    fun mapFromEntityList(entities: List<EnvFeedNetworkEntity>): List<EnvFeed> {
        return entities.map { mapFromEntity(it) }
    }
}