package com.rubenkuilder.wemamonitor.data.repository.currentWeather

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rubenkuilder.wemamonitor.data.db.CurrentWeatherDao
import com.rubenkuilder.wemamonitor.data.db.currentWeather.CurrentWeatherDBMapper
import com.rubenkuilder.wemamonitor.data.domain.currentWeather.CurrentWeather
import com.rubenkuilder.wemamonitor.data.network.currentWeather.CurrentWeatherNetworkMapper
import com.rubenkuilder.wemamonitor.data.network.currentWeather.WeatherNetworkDataSource
import com.rubenkuilder.wemamonitor.data.network.currentWeather.response.CurrentWeatherNetworkEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.ZonedDateTime

class CurrentWeatherRepositoryImpl(
    private val currentWeatherDao: CurrentWeatherDao,
    private val weatherNetworkDataSource: WeatherNetworkDataSource,
    private val currentWeatherDbMapper: CurrentWeatherDBMapper,
    private val currentWeatherNetworkMapper: CurrentWeatherNetworkMapper
) : CurrentWeatherRepository {

    private var hardenbergLastTimeFetch = ZonedDateTime.now().minusHours(1)
    private var bremenLastTimeFetch = ZonedDateTime.now().minusHours(1)

    /**
     * Initialize observable
     */
    init {
        weatherNetworkDataSource.downloadedCurrentWeatherNetworkEntity.observeForever { newCurrentWeather ->
            persistFetchedCurrentWeather(newCurrentWeather)
        }
    }

    /**
     * Check if data needs to be fetched from API and get weather data from database
     *
     * @param cityID the ID of the city to fetch
     */
    override suspend fun getCurrentWeather(cityID: Int): LiveData<CurrentWeather> {
        return withContext(Dispatchers.IO) {
            initWeatherData(cityID)

            val myWeatherData = MutableLiveData<CurrentWeather>(
                currentWeatherDbMapper.mapFromEntity(currentWeatherDao.getWeather(cityID))
            )
            return@withContext myWeatherData as LiveData<CurrentWeather>
        }
    }

    /**
     * Upsert weather data to database
     *
     * @param fetchedWeather the weather fetched in weatherNetworkDataSource
     */
    private fun persistFetchedCurrentWeather(fetchedWeather: CurrentWeatherNetworkEntity) {
        GlobalScope.launch(Dispatchers.IO) {
            Log.i("Test - ForecastRepositoryImpl", fetchedWeather.toString())

            val currentWeather = currentWeatherNetworkMapper.mapFromEntity(fetchedWeather)
            currentWeatherDao.upsertWeather(currentWeatherDbMapper.mapToEntity(currentWeather))
        }
    }

    /**
     * Check if fetch is needed based on city ID. If fetch is needed, upsert data into database and set timeFetched
     *
     * @param cityID the ID of the city to fetch
     */
    private suspend fun initWeatherData(cityID: Int) {
        if(isFetchCurrentNeeded(cityID)) {
            fetchCurrentWeather(cityID)

            //Set new fetched times
            when(cityID) {
                2754861 -> hardenbergLastTimeFetch = ZonedDateTime.now()
                2944388 -> bremenLastTimeFetch = ZonedDateTime.now()
            }
        }
    }

    /**
     * Fetch weather information of a city
     *
     * @param cityID the ID of the city to fetch
     */
    private suspend fun fetchCurrentWeather(cityID: Int) {
        weatherNetworkDataSource.fetchCurrentWeather(
            cityID,
            "metric"
        )
    }

    /**
     * Check if fetching of API data is currently needed
     *
     * @return return if timeFetched is before thirty minutes ago
     */
    private fun isFetchCurrentNeeded(cityID: Int): Boolean {
        val thirtyMinutesAgo = ZonedDateTime.now().minusMinutes(30)

        return when(cityID) {
            2754861 -> hardenbergLastTimeFetch!!.isBefore(thirtyMinutesAgo)
            2944388 -> bremenLastTimeFetch!!.isBefore(thirtyMinutesAgo)
            else -> true
        }
    }
}