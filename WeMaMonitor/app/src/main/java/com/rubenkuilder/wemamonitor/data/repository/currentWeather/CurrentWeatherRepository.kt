package com.rubenkuilder.wemamonitor.data.repository.currentWeather

import androidx.lifecycle.LiveData
import com.rubenkuilder.wemamonitor.data.domain.currentWeather.CurrentWeather

interface CurrentWeatherRepository {
    suspend fun getCurrentWeather(cityID: Int): LiveData<CurrentWeather>
}