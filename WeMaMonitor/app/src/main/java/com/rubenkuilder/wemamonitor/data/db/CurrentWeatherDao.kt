package com.rubenkuilder.wemamonitor.data.db

import androidx.room.*
import com.rubenkuilder.wemamonitor.data.db.currentWeather.entity.CurrentWeatherDBEntity
import com.rubenkuilder.wemamonitor.data.db.envFeed.entity.*

@Dao
interface CurrentWeatherDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertWeather(weatherDBEntity: CurrentWeatherDBEntity)

    @Query("SELECT * FROM current_weather WHERE cityID = :cityID LIMIT 1")
    fun getWeather(cityID: Int): CurrentWeatherDBEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertProductDBEntity(productDBEntity: ProductDBEntity): Long

    @Query("SELECT * FROM env_feed_product WHERE rowID = :id ")
    fun loadSingleProduct(id: Long): ProductDBEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertEnvironmentDBEntity(environmentDBEntity: EnvironmentDBEntity): Long

    @Query("SELECT * FROM env_feed_environment WHERE rowID = :id ")
    fun loadSingleEnvironment(id: Long): EnvironmentDBEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertProductWithFromEnv(join: ProductFromEnvPair)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertProductWithToEnv(join: ProductToEnvPair)

    @Transaction
    @Query("SELECT * FROM env_feed_product")
    fun getProductWithEnv(): List<ProductWithEnv>
}