package com.rubenkuilder.wemamonitor.views.pieChart

import android.graphics.Paint
import android.graphics.PointF

data class PieSlice(
    val name: String,
    var value: Double,
    var startAngle: Float,
    var sweepAngle: Float,
    val indicatorCircleLocation: PointF,
    val paint: Paint
)