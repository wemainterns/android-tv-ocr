package com.rubenkuilder.wemamonitor.data.db.envFeed.entity

import androidx.room.Entity

@Entity(primaryKeys = ["product_id", "env_id"])
data class ProductToEnvPair (
    val product_id: String,
    val env_id: String
)