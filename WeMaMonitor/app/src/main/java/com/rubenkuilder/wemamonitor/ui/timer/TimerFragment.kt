package com.rubenkuilder.wemamonitor.ui.timer

import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import com.rubenkuilder.wemamonitor.*
import com.rubenkuilder.wemamonitor.utilities.Utils.break1EndTime
import com.rubenkuilder.wemamonitor.utilities.Utils.break2EndTime
import com.rubenkuilder.wemamonitor.utilities.Utils.break3EndTime
import com.rubenkuilder.wemamonitor.utilities.Utils.isBreak
import com.rubenkuilder.wemamonitor.utilities.Utils.orderReminderEndTime
import com.rubenkuilder.wemamonitor.utilities.Utils.testEndTime
import com.rubenkuilder.wemamonitor.databinding.FragmentTimerBinding
import com.rubenkuilder.wemamonitor.utilities.TEST_TIMER_FLASH_IN_MS
import java.time.*
import java.time.temporal.ChronoUnit.MILLIS

/**
 * Fragment which shows a countdown timer
 */
class TimerFragment : Fragment() {
    private lateinit var binding: FragmentTimerBinding
    private lateinit var mediaPlayer: MediaPlayer

    private var timeDifference: Long = 0
    private var isFinished = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.i("Test", "---------------------")
        Log.i("Test - testTimerFragment", "Fragment view created")
        val fm: FragmentManager? = fragmentManager
        Log.i("Test - testTimerFragment", "backStackEntryCount: ${fm!!.backStackEntryCount}")

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_timer, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val currentDateTime = LocalDateTime.of(LocalDate.now(ZoneId.of("Europe/Amsterdam")), LocalTime.now(ZoneId.of("Europe/Amsterdam")))

        when(isBreak()) {
            0 -> {
                binding.fragmentTitle.text = "Goedemorgen,"
                binding.countdownTitle.text = "Tijd om te testen:"
                timeDifference = currentDateTime.toLocalTime().until(testEndTime, MILLIS)
            }
            1 -> {
                binding.fragmentTitle.text = "Pauze!"
                binding.countdownTitle.text = "Tijd voor pauze:"
                timeDifference = currentDateTime.toLocalTime().until(break1EndTime, MILLIS)
            }
            2 -> {
                binding.fragmentTitle.text = "Pauze!"
                binding.countdownTitle.text = "Tijd voor pauze:"
                timeDifference = currentDateTime.toLocalTime().until(break2EndTime, MILLIS)

                when (currentDateTime.dayOfWeek) {
                    DayOfWeek.WEDNESDAY -> {
                        binding.fallingImages.image = requireActivity().resources.getDrawable(R.drawable.gehaktbal)
                    }
                    DayOfWeek.FRIDAY -> {
                        binding.fallingImages.image = requireActivity().resources.getDrawable(R.drawable.lekkerbek)
                    }
                }
                binding.fallingImages.autoPlay = true
            }
            3 -> {
                binding.fragmentTitle.text = "Pauze!"
                binding.countdownTitle.text = "Tijd voor pauze:"
                timeDifference = currentDateTime.toLocalTime().until(break3EndTime, MILLIS)
            }
            4 -> {
                binding.fragmentTitle.text = "Bestel reminder!"
                binding.countdownTitle.text = "Vergeet niet te bestellen!"
                timeDifference = currentDateTime.toLocalTime().until(orderReminderEndTime, MILLIS)

                binding.fallingImages.image = requireActivity().resources.getDrawable(R.drawable.gehaktbal)

                binding.fallingImages.autoPlay = true
            }
        }

        timerHandler()
        audioHandler()
        flashHandler()
    }

    /**
     * Check if this fragment should be active or not
     * If fragment should be action, start the timer
     * Once timer has finished, navigate to serverStatusFragment
     */
    private fun timerHandler() {
        val timer = object : CountDownTimer(timeDifference, 1000) {
            override fun onTick(millisUntilFinished: Long) {
//                    Log.i("Test - testTimerFragment", (millisUntilFinished / 1000).toString())

                val minutes = millisUntilFinished / 1000 / 60
                val seconds = millisUntilFinished / 1000 % 60

                binding.countdown.text = "${minutes}:${String.format("%02d", seconds)}"
            }

            override fun onFinish() {
                Log.i("Test - testTimerFragment", "Timer finished")
                isFinished = true

                if(binding.fallingImages.autoPlay) {
                    binding.fallingImages.pause()
                }

                requireActivity().onBackPressed()
            }
        }
        timer.start()
    }

    /**
     * Handles the audio at the start of the fragment's life
     */
    private fun audioHandler() {
        if(isBreak() == 0) {
            mediaPlayer = MediaPlayer.create(requireContext(), R.raw.lets_get_ready_to_rumble)
            mediaPlayer.start()
        } else {
            mediaPlayer = MediaPlayer.create(requireContext(), R.raw.weeeee)
            mediaPlayer.start()
        }
    }

    /**
     * Handles the flash at the start of the fragment's life
     */
    private var i = 0
    private val mainHandler = Handler(Looper.getMainLooper())
    private fun flashHandler() {
        //Make sure a background is set
        Log.i("Test - TestTimerFragment", "frame.background = ${binding.frame.background != null}")

        if (i < 7 && !isFinished && binding.frame.background != null) {
            when (binding.frame.background.constantState) {
                activity?.getDrawable(R.color.red)?.constantState -> {
                    binding.frame.background = activity?.getDrawable(R.color.white)
                    Log.i("Test - TestTimerFragment", "should go white now")
                }
                else -> {
                    binding.frame.background = activity?.getDrawable(R.color.red)
                    Log.i("Test - TestTimerFragment", "should go red now")
                    Log.i("Test - TestTimerFragment", "${binding.frame.background}")
                }
            }
            Log.i("Test - TestTimerFragment", "$i")

            i++

            // Run code again after 5 seconds
            mainHandler.postDelayed(
                kotlinx.coroutines.Runnable { flashHandler() },
                TEST_TIMER_FLASH_IN_MS
            )
        } else {
            binding.frame.background = activity?.getDrawable(R.color.white)
        }
    }
}