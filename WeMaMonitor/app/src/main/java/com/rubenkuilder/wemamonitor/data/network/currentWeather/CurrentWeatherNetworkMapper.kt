package com.rubenkuilder.wemamonitor.data.network.currentWeather

import com.rubenkuilder.wemamonitor.data.domain.currentWeather.CurrentWeather
import com.rubenkuilder.wemamonitor.data.network.currentWeather.response.MainNetworkEntity
import com.rubenkuilder.wemamonitor.data.network.currentWeather.response.CurrentWeatherNetworkEntity
import com.rubenkuilder.wemamonitor.data.network.currentWeather.response.WeatherNetworkEntity
import com.rubenkuilder.wemamonitor.utilities.EntityMapper
import javax.inject.Inject

class CurrentWeatherNetworkMapper
@Inject
constructor() : EntityMapper<CurrentWeatherNetworkEntity, CurrentWeather> {
    override fun mapFromEntity(entity: CurrentWeatherNetworkEntity): CurrentWeather {
        return CurrentWeather(
            cityID = entity.cityID,
            weatherDesc = entity.weather[0].description,
            weatherIcon = entity.weather[0].icon,
            feelsLike = entity.main.feelsLike,
            humidity = entity.main.humidity,
            pressure = entity.main.pressure,
            temp = entity.main.temp,
            tempMax = entity.main.tempMax,
            tempMin = entity.main.tempMin
        )
    }

    override fun mapToEntity(domainModel: CurrentWeather): CurrentWeatherNetworkEntity {
        val weather = listOf(WeatherNetworkEntity(domainModel.weatherDesc, domainModel.weatherIcon))
        val currentWeatherNetwork = MainNetworkEntity(domainModel.feelsLike, domainModel.humidity, domainModel.pressure, domainModel.temp, domainModel.tempMax, domainModel.tempMin)

        return CurrentWeatherNetworkEntity(
            cityID = domainModel.cityID,
            weather = weather,
            main = currentWeatherNetwork
        )
    }
}