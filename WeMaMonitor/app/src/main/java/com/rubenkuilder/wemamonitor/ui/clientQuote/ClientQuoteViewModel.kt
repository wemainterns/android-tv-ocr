package com.rubenkuilder.wemamonitor.ui.clientQuote

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.rubenkuilder.wemamonitor.data.network.clientQuote.Quote
import com.rubenkuilder.wemamonitor.data.network.clientQuote.QuoteRepository

class ClientQuoteViewModel
@ViewModelInject
constructor(
    QuoteRepositoryImpl: QuoteRepository
): ViewModel() {
    private var _quote = MutableLiveData<Quote>()
    val quote: LiveData<Quote>
        get() = _quote

    init {
        _quote = QuoteRepositoryImpl.getQuote()
    }
}