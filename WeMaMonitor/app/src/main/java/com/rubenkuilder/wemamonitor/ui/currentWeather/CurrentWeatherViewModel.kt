package com.rubenkuilder.wemamonitor.ui.currentWeather

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.rubenkuilder.wemamonitor.data.repository.currentWeather.CurrentWeatherRepository
import com.rubenkuilder.wemamonitor.internal.lazyDeferred

class CurrentWeatherViewModel
@ViewModelInject
constructor(
    private val currentWeatherRepositoryImpl: CurrentWeatherRepository
): ViewModel() {

    val weatherHardenberg by lazyDeferred {
        currentWeatherRepositoryImpl.getCurrentWeather(2754861)
    }

    val weatherBremen by lazyDeferred {
        currentWeatherRepositoryImpl.getCurrentWeather(2944388)
    }
}