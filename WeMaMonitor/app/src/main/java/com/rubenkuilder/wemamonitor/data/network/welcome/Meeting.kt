package com.rubenkuilder.wemamonitor.data.network.welcome

data class Meeting(
    val clientName: String,
    val meetingTime: String,
    val clientImage: String
)