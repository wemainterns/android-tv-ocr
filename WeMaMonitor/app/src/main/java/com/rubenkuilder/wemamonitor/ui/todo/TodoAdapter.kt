package com.rubenkuilder.wemamonitor.ui.todo

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.rubenkuilder.wemamonitor.R
import com.rubenkuilder.wemamonitor.data.network.todo.Project
import java.lang.StringBuilder


class TodoAdapter(private val mContext: Context, val data: ArrayList<Project>): BaseAdapter() {
    override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
        val inflater = viewGroup?.context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.list_item_todo, null)

        val projectImageView: ImageView = view.findViewById(R.id.projectImage)
        val item = data[position]

        Glide.with(mContext).load(item.imageURL).into(projectImageView)

        return view
    }

    override fun getItem(p0: Int): Any? {
        return null
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return data.size
    }
}