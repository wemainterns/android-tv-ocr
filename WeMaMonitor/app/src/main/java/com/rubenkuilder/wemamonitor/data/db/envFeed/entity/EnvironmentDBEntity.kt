package com.rubenkuilder.wemamonitor.data.db.envFeed.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "env_feed_environment")
data class EnvironmentDBEntity (
    @PrimaryKey(autoGenerate = false)
    val env_id: String
)