package com.rubenkuilder.wemamonitor.data.network.currentWeather

import com.rubenkuilder.wemamonitor.data.network.ConnectivityInterceptor
import com.rubenkuilder.wemamonitor.data.network.currentWeather.response.CurrentWeatherNetworkEntity
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

const val API_KEY = "3f66171ba1937194c8e400f981278ca6"

//http://api.openweathermap.org/data/2.5/weather?q=Hardenberg&appid=3f66171ba1937194c8e400f981278ca6

interface OpenWeatherApiService {
    @GET("weather")
    suspend fun getCurrentWeather(
        @Query("id") cityID: Int,
        @Query("units") units: String = "metric"
    ): CurrentWeatherNetworkEntity

    /**
     * Make connection with the API URL
     */
    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor
        ): OpenWeatherApiService {
            val requestInterceptor = Interceptor {chain ->
                val url = chain.request()
                    .url()
                    .newBuilder()
                    .addQueryParameter("appid",
                        API_KEY
                    )
                    .build()

                val request = chain.request()
                    .newBuilder()
                    .url(url)
                    .build()

                return@Interceptor chain.proceed(request)
            }

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(requestInterceptor)
                .addInterceptor(connectivityInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(OpenWeatherApiService::class.java)
        }
    }
}