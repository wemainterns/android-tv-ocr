package com.rubenkuilder.wemamonitor.data.network.latestProject

data class LatestProject(
    val projectName: String,
    val projectImageURL: String,
    val clientName: String,
    val clientImageURL: String
) {
    override fun toString(): String {
        return "$clientName's '$projectName' app"
    }
}