package com.rubenkuilder.wemamonitor.data.repository.envFeed

import androidx.lifecycle.LiveData
import com.rubenkuilder.wemamonitor.data.domain.envFeed.EnvFeed

interface EnvFeedRepository {
    suspend fun getEnvFeed(): LiveData<List<EnvFeed>>
}