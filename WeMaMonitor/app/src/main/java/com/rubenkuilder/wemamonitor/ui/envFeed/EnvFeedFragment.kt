package com.rubenkuilder.wemamonitor.ui.envFeed

import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.rubenkuilder.wemamonitor.R
import com.rubenkuilder.wemamonitor.databinding.FragmentEnvFeedBinding
import com.rubenkuilder.wemamonitor.ui.todo.TodoAdapter
import com.rubenkuilder.wemamonitor.utilities.SCREEN_NAV_IN_MS
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.lang.StringBuilder
import kotlin.coroutines.CoroutineContext

/**
 * Fragment which shows the environment feed
 */
@AndroidEntryPoint
class EnvFeedFragment : Fragment(), CoroutineScope {
    lateinit var binding: FragmentEnvFeedBinding
    lateinit var viewModel: EnvFeedViewModel
    private lateinit var navController: NavController

    private val mainHandler = Handler()

    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_env_feed, container, false)
        viewModel = ViewModelProvider(this).get(EnvFeedViewModel::class.java)
        navController = findNavController()

        job = Job()

        bindUI()

        autoNavigateNext()

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun bindUI() = launch {
        val currentWeather = viewModel.envFeed.await()

        currentWeather.observe(viewLifecycleOwner, Observer {
            if (it == null) return@Observer

            val limitedDataReversed = it.reversed().take(10)

            binding.listView.adapter = EnvFeedAdapter(
                resources,
                limitedDataReversed
            )
        })
    }

    /**
     * Timer which handles navigation to ServerStatus fragment
     */
    private fun autoNavigateNext() {
        mainHandler.postDelayed({
            //Keep navigating
            mainHandler.removeCallbacksAndMessages(null)
            navController.navigate(R.id.action_envFeedFragment_to_serverStatusFragment)
        }, SCREEN_NAV_IN_MS)
    }

    override fun onPause() {
        super.onPause()
        mainHandler.removeCallbacksAndMessages(null)
    }

    override fun onDestroy() {
        job.cancel()
        super.onDestroy()
    }
}