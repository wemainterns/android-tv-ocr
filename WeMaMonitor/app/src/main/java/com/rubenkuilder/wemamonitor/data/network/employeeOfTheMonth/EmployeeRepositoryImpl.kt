package com.rubenkuilder.wemamonitor.data.network.employeeOfTheMonth

import androidx.lifecycle.MutableLiveData
import javax.inject.Inject

class EmployeeRepositoryImpl
@Inject
constructor(): EmployeeRepository {
    private var employee: Employee? = null

    /**
     * Pretend to get data from a webservice or online source
     */
    override fun getEmployee(): MutableLiveData<Employee> {
        setEmployee()

        val data = MutableLiveData<Employee>()
        data.value = employee

        return data
    }

    /**
     * Add employee
     */
    override fun setEmployee() {
        employee = Employee("Koen", "https://i.postimg.cc/dtGQHDyH/IMG-20200312-140228.jpg")
    }
}