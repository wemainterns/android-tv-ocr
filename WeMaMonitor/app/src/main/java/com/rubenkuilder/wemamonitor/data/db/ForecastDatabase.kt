package com.rubenkuilder.wemamonitor.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.rubenkuilder.wemamonitor.data.db.currentWeather.entity.CurrentWeatherDBEntity
import com.rubenkuilder.wemamonitor.data.db.envFeed.entity.EnvironmentDBEntity
import com.rubenkuilder.wemamonitor.data.db.envFeed.entity.ProductDBEntity
import com.rubenkuilder.wemamonitor.data.db.envFeed.entity.ProductFromEnvPair
import com.rubenkuilder.wemamonitor.data.db.envFeed.entity.ProductToEnvPair

@Database(
    entities = [CurrentWeatherDBEntity::class, ProductDBEntity::class, EnvironmentDBEntity::class, ProductFromEnvPair::class, ProductToEnvPair::class],
    version = 1
)
abstract class ForecastDatabase: RoomDatabase() {
    abstract fun currentWeatherDao(): CurrentWeatherDao

    companion object {
        @Volatile private var instance: ForecastDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, ForecastDatabase::class.java, "forecast.db").build()
    }
}