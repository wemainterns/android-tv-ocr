package com.rubenkuilder.wemamonitor.ui.envFeed

import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.rubenkuilder.wemamonitor.R
import com.rubenkuilder.wemamonitor.data.domain.envFeed.EnvFeed

class EnvFeedAdapter(val resources: Resources, val data: List<EnvFeed>): BaseAdapter() {
    override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
        val inflater = viewGroup?.context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.list_item_envfeed, null)

        val productLabelTV: TextView = view.findViewById(R.id.product_label)
        val productTV: TextView = view.findViewById(R.id.product)
        val fromToLabelTV: TextView = view.findViewById(R.id.from_to_label)
        val fromToTV: TextView = view.findViewById(R.id.from_to)

        val fromToEnvString = StringBuilder()
        for (env in data[position].from_env) {
            fromToEnvString.append("$env/")
        }
        for (env in data[position].to_env) {
            fromToEnvString.append("$env/")
        }

        if(position != 0) {
            productLabelTV.setTextColor(resources.getColor(R.color.colorPrimaryDark25))
            productTV.setTextColor(resources.getColor(R.color.colorPrimaryDark25))
            fromToLabelTV.setTextColor(resources.getColor(R.color.colorPrimaryDark25))
            fromToTV.setTextColor(resources.getColor(R.color.colorPrimaryDark25))
        }

        productTV.text = data[position].product
        fromToTV.text = fromToEnvString

        return view
    }

    override fun getItem(p0: Int): Any? {
        return null
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return data.size
    }
}