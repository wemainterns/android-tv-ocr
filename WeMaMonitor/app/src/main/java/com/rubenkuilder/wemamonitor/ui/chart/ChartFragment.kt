package com.rubenkuilder.wemamonitor.ui.chart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.rubenkuilder.wemamonitor.R
import com.rubenkuilder.wemamonitor.databinding.FragmentChartBinding
import com.rubenkuilder.wemamonitor.views.columnChart.ColumnData
import com.rubenkuilder.wemamonitor.views.pieChart.PieData

/**
 * ChartFragment class. This class isn't used in the actual application but was made for learning purposes.
 */
class ChartFragment : Fragment() {
    val pieData = PieData()
    val columnData = ColumnData()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentChartBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_chart, container, false)

        pieData.add("Ruben", 18.0, "#d32f2f")
        pieData.add("Koen", 4.0, "#512DA8")
        pieData.add("Martijn", 10.0, "#388E3C")
        pieData.add("Ruben2", 47.0)
        pieData.add("Ruben3", 15.0)
        pieData.add("Ruben4", 50.0)

        columnData.add("Ruben", 18.0, "#d32f2f")
        columnData.add("Koen", 4.0, "#512DA8")
        columnData.add("Martijn", 10.0, "#388E3C")
        columnData.add("Ruben2", 47.0)
        columnData.add("Ruben3", 15.0)
        columnData.add("Ruben4", 10.0)
//
//        binding.pieChart.setData(pieData)
//        binding.columnChart.setData(columnData)

        return binding.root
    }
}