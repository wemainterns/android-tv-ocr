package com.rubenkuilder.wemamonitor.utilities

import android.content.Context
import java.io.IOException
import java.time.*


object Utils {
    /**
     * Check if currentime is in between the set start and end times.
     * The set start and end times are declared in a constant; TEST_START_HOUR, TEST_START_MINUTE, TEST_END_HOUR, TEST_END_MINUTE
     *
     * @return boolean if currenttime is in between the set start and end times
     */

    /**
     * Set the time variables
     */
    val testStartTime: LocalTime = LocalTime.of(
        TEST_START_HOUR,
        TEST_START_MINUTE
    )
    val testEndTime: LocalTime = LocalTime.of(
        TEST_END_HOUR,
        TEST_END_MINUTE
    )
    val break1StartTime: LocalTime = LocalTime.of(
        BREAK1_START_HOUR,
        BREAK1_START_MINUTE
    )
    val break1EndTime: LocalTime = LocalTime.of(
        BREAK1_END_HOUR,
        BREAK1_END_MINUTE
    )
    val break2StartTime: LocalTime = LocalTime.of(
        BREAK2_START_HOUR,
        BREAK2_START_MINUTE
    )
    val break2EndTime: LocalTime = LocalTime.of(
        BREAK2_END_HOUR,
        BREAK2_END_MINUTE
    )
    val break3StartTime: LocalTime = LocalTime.of(
        BREAK3_START_HOUR,
        BREAK3_START_MINUTE
    )
    val break3EndTime: LocalTime = LocalTime.of(
        BREAK3_END_HOUR,
        BREAK3_END_MINUTE
    )
    val orderReminderStartTime: LocalTime = LocalTime.of(
        ORDERREMINDER_START_HOUR,
        ORDERREMINDER_START_MINUTE
    )
    val orderReminderEndTime: LocalTime = LocalTime.of(
        ORDERREMINDER_END_HOUR,
        ORDERREMINDER_END_MINUTE
    )

    var meetingDateTime: LocalDateTime? = null

    /**
     * Check if currentTime is a break
     *
     * @return Type of break
     * 0 = testen
     * 1 = break 1
     * 2 = break 2
     * 3 = break 3
     * 4 = order reminder
     */
    fun isBreak(): Int {
        val currentDateTime = LocalDateTime.of(LocalDate.now(ZoneId.of("Europe/Amsterdam")), LocalTime.now(ZoneId.of("Europe/Amsterdam")))

        return if(currentDateTime.toLocalTime().isAfter(break1StartTime) && currentDateTime.toLocalTime().isBefore(
                break1EndTime
            )) {
            1
        } else if(currentDateTime.toLocalTime().isAfter(break2StartTime) && currentDateTime.toLocalTime().isBefore(
                break2EndTime
            )) {
            2
        } else if(currentDateTime.toLocalTime().isAfter(break3StartTime) && currentDateTime.toLocalTime().isBefore(
                break3EndTime
            )) {
            3
        } else if(currentDateTime.toLocalTime().isAfter(orderReminderStartTime) && currentDateTime.toLocalTime().isBefore(
                orderReminderEndTime
            )) {
            4
        } else {
            0
        }
    }

    /**
     * Check if currentDateTime is between start and end times of 'testen' or 'break' & meetingTime is not between start and end times of 'testen' or 'break'
     *
     * @return Whether currentDateTime is between start and end times of 'testen' or 'break' & meetingTime is not between start and end times of 'testen' or 'break'
     */
    fun checkTime(): Boolean {
        return isTimeForTestOrBreak() && !isMeetingtimeBetweenTestOrBreakTimes()
    }

    /**
     * Check if currentDateTime is between start and end times of 'meeting'
     *
     * @return Whether currentDateTime is between start and end times of 'meeting'
     */
    fun checkMeeting(): Boolean {
        val currentDateTime = LocalDateTime.of(LocalDate.now(ZoneId.of("Europe/Amsterdam")), LocalTime.now(ZoneId.of("Europe/Amsterdam")))

        return if(meetingDateTime != null) {
            (currentDateTime.isAfter(
                meetingDateTime!!.minusMinutes(
                    SHOW_X_MIN_BEFORE_CLIENT_MEETING
                )) && currentDateTime.isBefore(
                meetingDateTime!!.plusMinutes(
                    END_X_MIN_AFTER_CLIENT_MEETING
                )))
        } else {
            false
        }
    }

    /**
     * Check if currentDateTime is between start and end times of 'testen' or 'break'
     *
     * @return Whether currentDateTime is between start and end times of 'testen' or 'break'
     */
    private fun isTimeForTestOrBreak(): Boolean {
        val currentDateTime = LocalDateTime.of(LocalDate.now(ZoneId.of("Europe/Amsterdam")), LocalTime.now(ZoneId.of("Europe/Amsterdam")))

        return (currentDateTime.toLocalTime().isAfter(testStartTime) && currentDateTime.toLocalTime().isBefore(
            testEndTime
        )) ||
                (currentDateTime.toLocalTime().isAfter(break1StartTime) && currentDateTime.toLocalTime().isBefore(
                    break1EndTime
                )) ||
                (currentDateTime.toLocalTime().isAfter(break2StartTime) && currentDateTime.toLocalTime().isBefore(
                    break2EndTime
                )) ||
                (currentDateTime.toLocalTime().isAfter(break3StartTime) && currentDateTime.toLocalTime().isBefore(
                    break3EndTime
                )) ||
                (currentDateTime.toLocalTime().isAfter(orderReminderStartTime) && currentDateTime.toLocalTime().isBefore(
                    orderReminderEndTime
                ))
    }

    /**
     * Check if meetingDateTime is between start and end times of 'testen' or 'break'
     *
     * @return Whether meetingDateTime is between start and end times of 'testen' or 'break'
     */
    private fun isMeetingtimeBetweenTestOrBreakTimes(): Boolean {
        if(meetingDateTime != null) {
            return (meetingDateTime!!.minusMinutes(
                SHOW_X_MIN_BEFORE_CLIENT_MEETING
            ).isAfter(
                LocalDateTime.of(
                    LocalDate.now(ZoneId.of("Europe/Amsterdam")),
                    testStartTime
                )
            ) && meetingDateTime!!.minusMinutes(
                SHOW_X_MIN_BEFORE_CLIENT_MEETING
            ).isBefore(
                LocalDateTime.of(
                    LocalDate.now(ZoneId.of("Europe/Amsterdam")),
                    testEndTime
                )
            ) ||
                    meetingDateTime!!.minusMinutes(
                        SHOW_X_MIN_BEFORE_CLIENT_MEETING
                    ).isAfter(
                        LocalDateTime.of(
                            LocalDate.now(ZoneId.of("Europe/Amsterdam")),
                            break1StartTime
                        )
                    ) && meetingDateTime!!.minusMinutes(
                SHOW_X_MIN_BEFORE_CLIENT_MEETING
            ).isBefore(
                LocalDateTime.of(
                    LocalDate.now(ZoneId.of("Europe/Amsterdam")),
                    break1EndTime
                )
            ) ||
                    meetingDateTime!!.minusMinutes(
                        SHOW_X_MIN_BEFORE_CLIENT_MEETING
                    ).isAfter(
                        LocalDateTime.of(
                            LocalDate.now(ZoneId.of("Europe/Amsterdam")),
                            break2StartTime
                        )
                    ) && meetingDateTime!!.minusMinutes(
                SHOW_X_MIN_BEFORE_CLIENT_MEETING
            ).isBefore(
                LocalDateTime.of(
                    LocalDate.now(ZoneId.of("Europe/Amsterdam")),
                    break2EndTime
                )
            ) ||
                    meetingDateTime!!.minusMinutes(
                        SHOW_X_MIN_BEFORE_CLIENT_MEETING
                    ).isAfter(
                        LocalDateTime.of(
                            LocalDate.now(ZoneId.of("Europe/Amsterdam")),
                            break3StartTime
                        )
                    ) && meetingDateTime!!.minusMinutes(
                SHOW_X_MIN_BEFORE_CLIENT_MEETING
            ).isBefore(
                LocalDateTime.of(
                    LocalDate.now(ZoneId.of("Europe/Amsterdam")),
                    break3EndTime
                )
            ) ||
                    meetingDateTime!!.minusMinutes(
                        SHOW_X_MIN_BEFORE_CLIENT_MEETING
                    ).isAfter(
                        LocalDateTime.of(
                            LocalDate.now(ZoneId.of("Europe/Amsterdam")),
                            orderReminderStartTime
                        )
                    ) && meetingDateTime!!.minusMinutes(
                SHOW_X_MIN_BEFORE_CLIENT_MEETING
            ).isBefore(
                LocalDateTime.of(
                    LocalDate.now(ZoneId.of("Europe/Amsterdam")),
                    orderReminderEndTime
                )
            ))
        } else {
            return false
        }
    }
}