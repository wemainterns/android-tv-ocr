package com.rubenkuilder.wemamonitor.data.db.currentWeather

import com.rubenkuilder.wemamonitor.data.db.currentWeather.entity.CurrentWeatherDBEntity
import com.rubenkuilder.wemamonitor.data.domain.currentWeather.CurrentWeather
import com.rubenkuilder.wemamonitor.utilities.EntityMapper
import javax.inject.Inject

class CurrentWeatherDBMapper
@Inject
constructor(): EntityMapper<CurrentWeatherDBEntity, CurrentWeather> {
    override fun mapFromEntity(entity: CurrentWeatherDBEntity): CurrentWeather {
        return CurrentWeather(
            entity.cityID,
            entity.weatherDesc,
            entity.weatherIcon,
            entity.feelsLike,
            entity.humidity,
            entity.pressure,
            entity.temp,
            entity.tempMax,
            entity.tempMin
        )
    }

    override fun mapToEntity(domainModel: CurrentWeather): CurrentWeatherDBEntity {
        return CurrentWeatherDBEntity(
            domainModel.cityID,
            domainModel.weatherDesc,
            domainModel.weatherIcon,
            domainModel.feelsLike,
            domainModel.humidity,
            domainModel.pressure,
            domainModel.temp,
            domainModel.tempMax,
            domainModel.tempMin)
    }

}