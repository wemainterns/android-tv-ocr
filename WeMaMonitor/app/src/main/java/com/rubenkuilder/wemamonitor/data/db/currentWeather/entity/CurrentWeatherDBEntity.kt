package com.rubenkuilder.wemamonitor.data.db.currentWeather.entity


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "current_weather")
data class CurrentWeatherDBEntity(
    @PrimaryKey(autoGenerate = false)
    var cityID: Int,
    var weatherDesc: String,
    var weatherIcon: String,
    @SerializedName("feels_like")
    val feelsLike: Double,
    val humidity: Int,
    val pressure: Int,
    val temp: Double,
    @SerializedName("temp_max")
    val tempMax: Double,
    @SerializedName("temp_min")
    val tempMin: Double
)