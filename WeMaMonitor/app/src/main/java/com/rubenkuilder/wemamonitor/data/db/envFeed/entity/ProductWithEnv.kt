package com.rubenkuilder.wemamonitor.data.db.envFeed.entity

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class ProductWithEnv(
    @Embedded
    val product: ProductDBEntity,
    @Relation(
        parentColumn = "product_id",
        entityColumn = "env_id",
        associateBy = Junction(ProductFromEnvPair::class)
    )
    val from_environments: List<EnvironmentDBEntity>,
    @Relation(
        parentColumn = "product_id",
        entityColumn = "env_id",
        associateBy = Junction(ProductToEnvPair::class)
    )
    val to_environments: List<EnvironmentDBEntity>
)