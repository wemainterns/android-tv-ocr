package com.rubenkuilder.wemamonitor.data.network.todo

data class Project(
    val projectName: String,
    val status: Int,
    val imageURL: String
) {
    override fun toString(): String {
        return "$projectName has a status of $status"
    }
}