package com.rubenkuilder.wemamonitor.views.fallingImages

import android.animation.TimeAnimator
import android.animation.TimeAnimator.TimeListener
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.View
import com.rubenkuilder.wemamonitor.R
import kotlin.random.Random


class FallingImages @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : View(context, attrs, defStyleAttr) {
    // Data
    private var balls = ArrayList<Image>()
    val paint = Paint()

    private var mTimeAnimator: TimeAnimator? = null
    private var mCurrentPlayTime: Long? = null
    private val numberOfBalls: Int = 50
    private val attributes = context.obtainStyledAttributes(attrs, R.styleable.FallingImages)
    var image = attributes.getDrawable(R.styleable.FallingImages_image)
    var autoPlay = false

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        for(i in 0..numberOfBalls) {
            balls.add(Image((Random.nextInt(0, width - 20)).toFloat(), (0 - 200).toFloat(), 40, image!! , context))
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        for(ball in balls) {
            if(ball.y + ball.myBall.height < 0 || ball.y - ball.myBall.height > height) {
                continue
            }
            canvas.drawBitmap(ball.myBall, ball.x, ball.y, paint)
        }
        invalidate()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        mTimeAnimator = TimeAnimator()
        mTimeAnimator!!.setTimeListener(TimeListener { _, _, deltaTime ->
            if (!isLaidOut) {
                // Ignore all calls before the view has been measured and laid out.
                return@TimeListener
            }

            updateState(deltaTime.toFloat())
            invalidate()
        })
        start()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()

        stop()
    }

    fun start() {
        if(mTimeAnimator != null && autoPlay) {
            mTimeAnimator!!.start()
        }
    }

    fun pause() {
        if(mTimeAnimator != null && mTimeAnimator!!.isRunning) {
            mCurrentPlayTime = mTimeAnimator!!.currentPlayTime
            mTimeAnimator!!.pause()
        }
    }

    fun resume() {
        if(mTimeAnimator != null && mTimeAnimator!!.isPaused) {
            mTimeAnimator!!.start()
            mTimeAnimator!!.currentPlayTime = mCurrentPlayTime!!
        }
    }

    private fun stop() {
        if(mTimeAnimator != null) {
            mTimeAnimator!!.cancel()
            mTimeAnimator!!.setTimeListener(null)
            mTimeAnimator!!.removeAllListeners()
            mTimeAnimator = null
        }
    }

    /**
     * Progress the animation by moving the stars based on the elapsed time
     * @param deltaMs time delta since the last frame, in millis
     */
    private fun updateState(deltaMs: Float) {
        // Converting to seconds since PX/S constants are easier to understand
        val deltaSeconds = deltaMs / 1000f
        var idkWhatToCallThis = false
        for(ball in balls) {
            if(ball.y < height) {
                idkWhatToCallThis = true
            }
        }

        if(idkWhatToCallThis) {
            for (ball in balls) {
                // Move the star based on the elapsed time and it's speed
                ball.speed = ball.speed + 2
                ball.y += ball.speed * deltaSeconds
                Log.i("FallingImages", "${ball.speed}")
                Log.i("FallingImages", "${ball.y}")
            }
        } else {
            stop()
        }
    }
}