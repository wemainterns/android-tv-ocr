package com.rubenkuilder.wemamonitor.ui

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rubenkuilder.wemamonitor.data.network.welcome.Meeting
import com.rubenkuilder.wemamonitor.data.network.welcome.WelcomeRepository
import com.rubenkuilder.wemamonitor.data.network.welcome.WelcomeRepositoryImpl

class MainActivityViewModel
@ViewModelInject
constructor(
    WelcomeRepositoryImpl: WelcomeRepository
): ViewModel() {
    private var _meeting = MutableLiveData<Meeting>()
    val meeting: LiveData<Meeting>
        get() = _meeting

    init {
        _meeting = WelcomeRepositoryImpl.getMeeting()
    }
}