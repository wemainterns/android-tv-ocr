package com.rubenkuilder.wemamonitor.ui.video

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.rubenkuilder.wemamonitor.R
import com.rubenkuilder.wemamonitor.databinding.FragmentVideoBinding

/**
 * Fragment which shows a video
 */
class VideoFragment : Fragment() {
    private lateinit var binding: FragmentVideoBinding
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_video, container, false)
        navController = findNavController()

        // set the absolute path of the video file which is going to be played
        binding.videoContainer.setVideoURI(Uri.parse("android.resource://" + requireActivity().packageName + "/" + R.raw.video_tv_met_logo_2))

        binding.videoContainer.requestFocus()

        // starting the video
        binding.videoContainer.start()

        // navigate to next screen once video has finished
        binding.videoContainer.setOnCompletionListener {
            navController.navigate(R.id.action_videoFragment_to_latestProjectFragment)
        }

        return binding.root
    }

    override fun onPause() {
        super.onPause()

        // stopping the video
        binding.videoContainer.pause()
    }
}