package com.rubenkuilder.wemamonitor.data.network.currentWeather

import androidx.lifecycle.LiveData
import com.rubenkuilder.wemamonitor.data.network.currentWeather.response.CurrentWeatherNetworkEntity

interface WeatherNetworkDataSource {
    val downloadedCurrentWeatherNetworkEntity: LiveData<CurrentWeatherNetworkEntity>

    suspend fun fetchCurrentWeather(
        cityID: Int,
        units: String
    )
}