package com.rubenkuilder.wemamonitor.ui.todo

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.rubenkuilder.wemamonitor.*
import com.rubenkuilder.wemamonitor.databinding.FragmentTodoBinding
import com.rubenkuilder.wemamonitor.utilities.SCREEN_NAV_IN_MS
import dagger.hilt.android.AndroidEntryPoint

/**
 * Fragment which shows a to-do list
 */
@AndroidEntryPoint
class TodoFragment : Fragment() {
    private lateinit var binding: FragmentTodoBinding
    private lateinit var viewModel: TodoViewModel
    private lateinit var navController: NavController

    private val mainHandler = Handler()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.i("Test", "---------------------")
        Log.i("Test - TodoFragment", "Fragment view created")
        val fm: FragmentManager? = fragmentManager
        Log.i("Test - TodoFragment", "backStackEntryCount: ${fm!!.backStackEntryCount}")

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_todo, container, false)

        // Initialize variables
        viewModel = ViewModelProvider(this).get(TodoViewModel::class.java)
        navController = findNavController()

        viewModel.projectsDoing.observe(viewLifecycleOwner, Observer { projectsDoing ->
            Log.i("Test - projects", "$projectsDoing")
            binding.doingGrid.adapter = TodoAdapter(
                this.requireContext(),
                projectsDoing
            )
        })
        viewModel.projectsTesting.observe(viewLifecycleOwner, Observer { projectsTesting ->
            Log.i("Test - projects", "$projectsTesting")
            binding.testingGrid.adapter = TodoAdapter(
                this.requireContext(),
                projectsTesting
            )
        })
        viewModel.projectsDone.observe(viewLifecycleOwner, Observer { projectsDone ->
            Log.i("Test - projects", "$projectsDone")
            binding.doneGrid.adapter = TodoAdapter(
                this.requireContext(),
                projectsDone
            )
        })


        autoNavigateNext()

        return binding.root
    }

    /**
     * Timer which handles navigation to employeeOfTheMonth fragment
     */
    private fun autoNavigateNext() {
        mainHandler.postDelayed({
                //Keep navigating
                Log.i("Test - TodoFragment", "Keep navigating")
                Log.i("Test - TodoFragment", "Navigating from TodoFragment to EmployeeOfTheMonthFragment")
                mainHandler.removeCallbacksAndMessages(null)
                navController.navigate(R.id.action_todoFragment_to_employeeOfTheMonthFragment)
        }, SCREEN_NAV_IN_MS)
    }

    override fun onPause() {
        super.onPause()
        Log.i("Test - TodoFragment", "onDestroyView")
        mainHandler.removeCallbacksAndMessages(null)
    }
}