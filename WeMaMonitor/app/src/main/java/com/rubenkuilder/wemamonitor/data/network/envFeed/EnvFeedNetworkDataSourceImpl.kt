package com.rubenkuilder.wemamonitor.data.network.envFeed

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rubenkuilder.wemamonitor.data.network.envFeed.response.EnvFeedNetworkEntity
import com.rubenkuilder.wemamonitor.internal.NoConnectivityException

class EnvFeedNetworkDataSourceImpl(
    private val envFeedApiService: EnvFeedApiService
) : EnvFeedNetworkDataSource {
    private val _downloadedEnvFeed = MutableLiveData<List<EnvFeedNetworkEntity>>()
    override val downloadedEnvFeedNetworkEntity: LiveData<List<EnvFeedNetworkEntity>>
        get() = _downloadedEnvFeed

    /**
     * Fetch EnvFeed from the envFeedApiService
     */
    override suspend fun fetchEnvFeed() {
        try {
            val fetchedEnvFeed = envFeedApiService.getEnvFeed()

            Log.i("Test - EnvFeedDataSourceImpl", fetchedEnvFeed.toString())

            _downloadedEnvFeed.postValue(fetchedEnvFeed)
        } catch (e: NoConnectivityException) {
            Log.e("Connectivity", "No internet connection.", e)
        }
    }
}