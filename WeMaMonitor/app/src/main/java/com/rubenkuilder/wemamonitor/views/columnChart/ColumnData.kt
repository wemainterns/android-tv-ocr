package com.rubenkuilder.wemamonitor.views.columnChart

import android.graphics.Color
import android.graphics.Paint
import android.graphics.PointF
import java.util.*
import kotlin.collections.HashMap

class ColumnData {
    val columnBars = HashMap<String, ColumnBar>()
    var maxValue = 0.0

    /**
     * Adds data to the columnBars hashmap
     *
     * @param name the name of the item being added
     * @param value the value of the item being added
     * @param color the color of the item should be represented as (if not already in the map)
     */
    fun add(name: String, value: Double, color: String? = null) {
        if (columnBars.containsKey(name)) {
            columnBars[name]?.let { it.value += value }
        } else {
            color?.let {
                columnBars[name] = ColumnBar(name, value, 0f, 0f, 0f, PointF(), createPaint(it))
            } ?: run {
                columnBars[name] = ColumnBar(name, value, 0f, 0f, 0f, PointF(), createPaint(null))
            }
        }

        if (value > maxValue) {
            maxValue = value
        }
    }

    /**
     * Dynamically create paints for a given project
     * If no color is passed, we assign a random color
     *
     * @param color the color of the pain to create
     */
    private fun createPaint(color: String?): Paint {
        val newPaint = Paint()
        color?.let {
            newPaint.color = Color.parseColor(color)
        } ?: run {
            val randomValue = Random()
            newPaint.color = Color.argb(255, randomValue.nextInt(255), randomValue.nextInt(255), randomValue.nextInt(255))
        }
        newPaint.isAntiAlias = true

        return newPaint
    }
}