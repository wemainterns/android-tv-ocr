package com.rubenkuilder.wemamonitor.ui.clientQuote

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.rubenkuilder.wemamonitor.R
import com.rubenkuilder.wemamonitor.utilities.SCREEN_NAV_IN_MS
import com.rubenkuilder.wemamonitor.databinding.FragmentClientQuoteBinding
import dagger.hilt.android.AndroidEntryPoint

/**
 * Fragment which shows a random client quote
 */
@AndroidEntryPoint
class ClientQuoteFragment : Fragment() {
    private lateinit var binding: FragmentClientQuoteBinding

    private lateinit var viewModel: ClientQuoteViewModel
    private lateinit var navController: NavController

    private val mainHandler = Handler()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.i("Test", "---------------------")
        Log.i("Test - ClientQuoteFragment", "Fragment view created")
        val fm: FragmentManager? = fragmentManager
        Log.i("Test - ClientQuoteFragment", "backStackEntryCount: ${fm!!.backStackEntryCount}")

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_client_quote, container, false)

        // Initialize variables
        viewModel = ViewModelProvider(this).get(ClientQuoteViewModel::class.java)
        navController = findNavController()

        viewModel.quote.observe(viewLifecycleOwner, Observer { quote ->
            Glide.with(this).load(quote.imageURL).into(binding.clientImage)
            binding.quote.text = getString(R.string.quote, quote.quote)
            binding.quoteCredit.text = getString(R.string.quoteCredit, quote.name)
        })

        autoNavigateNext()

        return binding.root
    }

    /**
     * Timer which handles navigation to ServerStatusFragment
     */
    private fun autoNavigateNext() {
        mainHandler.postDelayed({
                //Keep navigating
                Log.i("Test - ClientQuoteFragment", "Keep navigating")
                Log.i("Test - ClientQuoteFragment", "Navigating from ClientQuoteFragment to VideoFragment")
                mainHandler.removeCallbacksAndMessages(null)
                navController.navigate(R.id.action_clientQuoteFragment_to_videoFragment)
        }, SCREEN_NAV_IN_MS)
    }

    override fun onPause() {
        super.onPause()
        Log.i("Test - ClientQuoteFragment", "onDestroyView")
        mainHandler.removeCallbacksAndMessages(null)
    }
}