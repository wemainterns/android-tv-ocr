package com.rubenkuilder.wemamonitor.ui.welcome

import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import com.rubenkuilder.wemamonitor.R
import com.rubenkuilder.wemamonitor.utilities.Utils.checkMeeting
import java.util.*

class WelcomeTimerTask(private val clientName: String, private val navController: NavController) : TimerTask() {
    override fun run() {
        if(checkMeeting()) {
            val navOptions = NavOptions.Builder().setPopUpTo(R.id.welcomeFragment, true).build()
            val bundle = bundleOf("clientName" to clientName)
            navController.navigate(R.id.welcomeFragment, bundle, navOptions)
        }
    }
}