package com.rubenkuilder.wemamonitor.data.db.envFeed

import com.rubenkuilder.wemamonitor.data.db.envFeed.entity.EnvironmentDBEntity
import com.rubenkuilder.wemamonitor.data.db.envFeed.entity.ProductDBEntity
import com.rubenkuilder.wemamonitor.data.db.envFeed.entity.ProductFromEnvPair
import com.rubenkuilder.wemamonitor.data.db.envFeed.entity.ProductWithEnv
import com.rubenkuilder.wemamonitor.data.domain.envFeed.EnvFeed
import com.rubenkuilder.wemamonitor.utilities.EntityMapper
import javax.inject.Inject

class EnvFeedDBMapper
@Inject
constructor(): EntityMapper<ProductFromEnvPair, EnvFeed>{

    // GET
    private fun mapProductWithFromEnvToEnvFeed(entity: ProductWithEnv): EnvFeed {
        var tempFromEnvList: MutableList<String> = mutableListOf()
        var tempToEnvList: MutableList<String> = mutableListOf()

        entity.from_environments.forEach {
            tempFromEnvList.add(it.env_id)
        }

        entity.to_environments.forEach {
            tempToEnvList.add(it.env_id)
        }

        return EnvFeed(
            product_id = entity.product.product_id,
            product = entity.product.product,
            employee = entity.product.employee,
            from_env = tempFromEnvList,
            to_env = tempToEnvList,
            date = entity.product.date,
            changelog = entity.product.changelog
        )
    }

    override fun mapFromEntity(entity: ProductFromEnvPair): EnvFeed {
        TODO("Not yet implemented")
    }

    override fun mapToEntity(domainModel: EnvFeed): ProductFromEnvPair {
        TODO("Not yet implemented")
    }

    fun mapFromProductWithFromEnvListToEnvFeed(entities: List<ProductWithEnv>): List<EnvFeed> {
        return entities.map { mapProductWithFromEnvToEnvFeed(it) }
    }

    fun mapEnvFeedToProductDBEntity(envFeed: EnvFeed): ProductDBEntity {
        return ProductDBEntity(
            envFeed.product_id,
            envFeed.product,
            envFeed.employee,
            envFeed.date,
            envFeed.changelog)
    }

    fun mapEnvToEnvironmentDBEntity(env: String): EnvironmentDBEntity {
        return EnvironmentDBEntity(env)
    }
}