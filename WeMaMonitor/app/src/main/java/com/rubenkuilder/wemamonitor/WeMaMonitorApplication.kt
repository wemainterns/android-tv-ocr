package com.rubenkuilder.wemamonitor

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WeMaMonitorApplication: Application() {
}